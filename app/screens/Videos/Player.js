import React, { Component } from 'react';
import { Dimensions, StatusBar, View } from 'react-native';
import VideoPlayer from 'react-native-video-player';

// import Video from 'react-native-video';
const styles = require('../../config/styles');
const colors = require('../../config/colors');
const sentenceCase = require('sentence-case');
const uuidv1 = require('uuid/v1');
export default class Player extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    onLayout(e) {
        const {width, height} = Dimensions.get('window')
        console.log(width, height)
      }

    _keyExtractor = () => uuidv1();
    componentDidMount() {
        // this.player.presentFullscreenPlayer()
    }
    render() {
        const { data } = this.state;
        return (
                <View style={{justifyContent: 'center', flex:1, backgroundColor: 'black'}}>
                    <StatusBar hidden={true} />
                    <VideoPlayer
                    video={this.props.src}
                    fullScreenOnLongPress={true}
                    ref={r => this.player = r}
                    resizeMode="contain"
                    autoplay={true}
                    loop={true}
                    />
                </View>


        )
    }
}