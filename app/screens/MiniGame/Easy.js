import async from 'async';
import _ from 'lodash';
import React, { Component } from 'react';
import { AppState, AsyncStorage, Dimensions, ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import CountDown from 'react-native-countdown-component';
import RNFirebase from 'react-native-firebase';
import { ActionConst, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const shuffle = require('knuth-shuffle').knuthShuffle;
const alphas = require('../../config/data/alphabets').data
const nums = require('../../config/data/numbers').data
const alphanums = alphas.concat(nums)
const questionBucket = shuffle(alphanums).splice(0, 21)
const width = Dimensions.get('window').width; //full width
const uuidv4 = require('uuid/v4')
const sentenceCase = require('sentence-case')
const colors = require('../../config/colors')
const Sound = require('react-native-sound');

const firebase = RNFirebase.app();

const leaderboardRef = firebase.database().ref('leaderboard')

class Easy extends Component {
    constructor(props) {
        super(props)
        this.state = {
            questions: [],
            index: 1,
            countdownComponent: undefined,
            score: 0,
            correct: true,
            playername: 'default'
        }
    }

    playCorrect() {
        this.correctSound.play((success) => {
            // this.sound.release();
            if (success) {
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
            }
        });
    }

    playWrong() {
        this.wrongSound.play((success) => {
            // this.sound.release();
            if (success) {
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
            }
        });
    }

    componentDidMount() {
        // leaderboardRef.once("value").then(s=>{
        //     console.log("==============================")
        //     console.log(s.val())
        // })
        let questions = []
        let randomNumber = shuffle(_.range(0, questionBucket.length))
        
        const app = this
        const { correct } = this.state

        AsyncStorage.getItem('@MySuperStore:deviceuser').then(value=>{
            const res = JSON.parse(value).playername
            console.log(res)
            app.setState({playername: res})
        })

        async.forEachOf(questionBucket, function (value, key, callback) {
            let randomSubNumber = new shuffle(_.range(0, questionBucket.length))
            const optionList = [
                questionBucket[randomSubNumber[0]].text===value.text?questionBucket[randomSubNumber[2]].text:questionBucket[randomSubNumber[0]].text,
                questionBucket[randomSubNumber[7]].text===value.text?questionBucket[randomSubNumber[8]].text:questionBucket[randomSubNumber[7]].text,
                questionBucket[randomSubNumber[17]].text===value.text?questionBucket[randomSubNumber[18]].text:questionBucket[randomSubNumber[17]].text,
                value.text
            ]

            const shuffledOption = shuffle(optionList);

            const questionNumbers = [0, 1, 2, 3]

            const shuffledQuestionNumbers = shuffle(questionNumbers)

            questions.push(
                {
                    identify: value.image,
                    options: [{
                        a: shuffledOption[shuffledQuestionNumbers[0]],
                        b: shuffledOption[shuffledQuestionNumbers[1]],
                        c: shuffledOption[shuffledQuestionNumbers[2]],
                        d: shuffledOption[shuffledQuestionNumbers[3]],
                    }],
                    answer: value.text,
                    countdown: <CountDown
                        key={uuidv4()}
                        until={15}
                        onFinish={() => app.incrementIndex()}
                        size={20}
                        timeToShow={["S"]}
                        digitTxtColor="#FFF"
                        digitBgColor={colors.amber['700']}
                        style={{ marginTop: 10 }}
                        timeTxtColor="#FFF"
                    />
                }
            )
            callback()
        }, (err) => {
            if (!err) this.setState({ questions: questions });
        })

        this.wrongSound = new Sound(require('../../assets/wrong.wav'), Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + test.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        });

        this.correctSound = new Sound(require('../../assets/correct.wav'), Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + test.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        });

        AppState.addEventListener('change', (state) => {
            if (state === 'background') {
                this.correctSound.stop()
                this.wrongSound.stop()
            }
        })

    }

    incrementIndex = () => {
        const { index, questions, score, playername } = this.state;
        // if(!index>questions.length){
        //     this.setState({index: index+1});
        //     console.log(index)
        // } 
        console.log(playername)
        if (index < questions.length - 1) {
            this.setState({ index: index + 1, correct: true });
        } else {
            this.setState({ index: null });
            console.log("Updating db: "+_.snakeCase(playername))
            leaderboardRef.child(_.snakeCase(playername)).update({
                easy:score, 
                name: playername
            })
        }

    }

    validateAnswer(answer, correctAnswer) {
        let { score, index, playername } = this.state
        //alert(`answer: ${answer},  correct: ${correctAnswer}`)
        console.log(playername)
        if (answer === correctAnswer) {
            this.setState({ score: score += 1, index: index += 1, correct: true })
            this.playCorrect()
        } else {
            this.playWrong()
            this.setState({ correct: false })
            this.incrementIndex()
            setTimeout(() => {
                this.setState({ correct: true })
            }, 2000)
        }
        console.log(index)
        if(index===null){
            console.log("Updating db: "+_.snakeCase(playername))
            leaderboardRef.child(_.snakeCase(playername)).update({
                easy:score, 
                name: playername
            })
        }
    }

    render() {
        const { questions, index, score, correct } = this.state;
        let currentIndex = parseInt(index);
        let feedback = ""
        let stars = []

        if (score <= 5) {
            feedback = "Better luck next time!"
            stars = (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="star-o" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star-o" style={{ fontSize: 90, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star-o" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                </View>
            )
        }

        if (score >= 6 && score <= 10) {
            feedback = "Better luck next time!"
            stars = (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="star" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star-o" style={{ fontSize: 90, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star-o" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                </View>
            )
        }

        if (score >= 11 && score <= 15) {
            feedback = "Well done!"
            stars = (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="star" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star" style={{ fontSize: 90, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star-o" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                </View>
            )
        }

        if (score >= 16 && score <= 20) {
            feedback = "Congratulations!"
            stars = (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon name="star" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star" style={{ fontSize: 90, margin: 5, color: colors.yellow['700'] }} />
                    <Icon name="star" style={{ fontSize: 60, margin: 5, color: colors.yellow['700'] }} />
                </View>
            )
        }



        return (
            <KeyboardAwareScrollView>
            <ScrollView contentContainerStyle={{backgroundColor: '#333', flexGrow:1, flex:1}}>
                <View style={{flex:1, marginBottom: questions[index] !== undefined ?'20%':0}}>
                    {questions[index] !== undefined ? (
                        <View style={{flex:1}}>
                        <View style={{ flexDirection: 'row', padding: 15, backgroundColor: correct ? '#333' : 'red', marginBottom: 20}}>
                                <Text style={{ color: '#fff', flex: 1, fontSize: 15, textAlign: 'center' }}>Items: {index + "/" + `${questions.length - 1}`}</Text>
                                <Text style={{ color: '#fff', flex: 1, fontSize: 15, textAlign: 'center' }}>Score: {score}</Text>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                <ImageBackground style={{ width: width - 100, height: width - 100 }} source={questions[currentIndex].identify} resizeMode="cover">
                                    <View style={{position: 'absolute', bottom: 10, right: 20}}>
                                    {questions[currentIndex].countdown}
                                    </View>
                                </ImageBackground>
                            </View>

                            {
                                Object.keys(questions[currentIndex].options[0]).map(key => (
                                    <TouchableOpacity style={{ padding: 15, backgroundColor: '#fff', margin: 3 }} key={uuidv4()} onPress={() => this.validateAnswer(questions[currentIndex].options[0][key], questions[currentIndex].answer)}>
                                        <Text style={{ fontSize: 13 }}>{sentenceCase(questions[currentIndex].options[0][key])}</Text>
                                    </TouchableOpacity>
                                ))
                            }                        
                        </View>
                    ) :
                        <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center', padding: 50 }}>
                        <View style={{backgroundColor: '#fff', margin: '10%', padding: 30, borderRadius: 20}}>   
                        <Text style={{ textAlign: 'center', marginBottom: 0 }}>You scored</Text>                                             
                            <Text style={{ textAlign: 'center', fontSize: 50, fontWeight: 'bold' }}>{`${score}/${questions.length-1}`}</Text>
                            {stars}
                            <Text style={{ textAlign: 'center', fontSize: 23 }}>{feedback}</Text>                            
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity style={[{ backgroundColor: colors.primary, justifyContent: 'center', width: 60, height: 60, borderRadius: 30, alignContent: 'center', margin: 5 }]} onPress={()=>Actions.leaderboard({type: ActionConst.REPLACE})}>
                                    <Icon name="signal" style={{ color: colors.white, textAlign: 'center', fontSize: 30 }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[{ backgroundColor: colors.primary, justifyContent: 'center', width: 60, height: 60, borderRadius: 30, alignContent: 'center', margin: 5 }]} onPress={()=>Actions.easy({type: ActionConst.REPLACE})}>
                                    <Icon name="refresh" style={{ color: colors.white, textAlign: 'center', fontSize: 30 }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[{ backgroundColor: colors.primary, justifyContent: 'center', width: 60, height: 60, borderRadius: 30, alignContent: 'center', margin: 5 }]}>
                                    <Icon name="bars" style={{ color: colors.white, textAlign: 'center', fontSize: 30 }} onPress={()=>Actions.startgamecategories({type: ActionConst.REPLACE})} />
                                </TouchableOpacity>
                            </View>
                            </View>
                            <Text style={{ textAlign: 'center', marginBottom: 20, color: '#FFF' }}>Game Finished!</Text>
                        </View>
                    }                    
                </View>
            </ScrollView>
            </KeyboardAwareScrollView>

        );
    }
}

export default Easy;