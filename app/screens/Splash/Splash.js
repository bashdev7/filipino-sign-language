import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import { ActionConst, Actions } from 'react-native-router-flux';

const styles = require('../../config/styles.js');
const settings = require('../../config/settings.js');

export default class Splash extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    Actions.onboarding({ type: ActionConst.REPLACE })
    // firebase.auth().onAuthStateChanged(function (user) {
    //   if (user) {
    //     Actions.home({ type: ActionConst.REPLACE })
    //   } else {
    //     Actions.login({ type: ActionConst.REPLACE })
    //   }
    // });
  }


  render() {
    return (
      <View style={styles.splashView}>
        <Image
          style={{ width: 80, height: 80 }}
          source={require('../../assets/app-logo.png')}
          resizeMode="contain"
        />
        <Text style={{ margin: 20, color: '#FFF' }}>Loading...</Text>
      </View>
    )
  }
}
