const uuidv1 = require('uuid/v1');
module.exports = {
    data: [
        {
            id: uuidv1(),
            text: "1",
            image: require('../../assets/animated-sign-language/numbers/1.jpg')
        },
        {
            id: uuidv1(),
            text: "2",
            image: require('../../assets/animated-sign-language/numbers/2.jpg')
        },
        {
            id: uuidv1(),
            text: "3",
            image: require('../../assets/animated-sign-language/numbers/3.jpg')
        },
        {
            id: uuidv1(),
            text: "4",
            image: require('../../assets/animated-sign-language/numbers/4.jpg')
        },
        {
            id: uuidv1(),
            text: "5",
            image: require('../../assets/animated-sign-language/numbers/5.jpg')
        },
        {
            id: uuidv1(),
            text: "6",
            image: require('../../assets/animated-sign-language/numbers/6.jpg')
        },
        {
            id: uuidv1(),
            text: "7",
            image: require('../../assets/animated-sign-language/numbers/7.jpg')
        },
        {
            id: uuidv1(),
            text: "8",
            image: require('../../assets/animated-sign-language/numbers/8.jpg')
        },
        {
            id: uuidv1(),
            text: "9",
            image: require('../../assets/animated-sign-language/numbers/9.jpg')
        },
        {
            id: uuidv1(),
            text: "10",
            image: require('../../assets/animated-sign-language/numbers/10.jpg')
        },
    ]
}