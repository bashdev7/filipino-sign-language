./gradlew assembleRelease
~/Android/Sdk/build-tools/26.0.2/zipalign -v -p 4 ./app/build/outputs/apk/release/app-release-unsigned.apk my-app-unsigned-aligned.apk
~/Android/Sdk/build-tools/26.0.2/apksigner sign --ks bluqube.jks --out my-app-release-unsigned-signed.apk my-app-unsigned-aligned.apk
rm my-app-unsigned-aligned.apk
mv my-app-release-unsigned-signed.apk release.apk

