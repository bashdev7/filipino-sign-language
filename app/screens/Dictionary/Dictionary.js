import React, { Component } from 'react';
import { FlatList, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Icon from 'react-native-vector-icons/Feather';

const dictionary = require('../../config/data/dictionary').words_phrases
const animal_signs = require('../../config/data/animal_signs');
const basic_questions = require('../../config/data/basic_questions');
const beginning_signs = require('../../config/data/beginning_signs');
const body_parts = require('../../config/data/body_parts');
const behavior_and_safety = require('../../config/data/behavior_and_safety');
const categories = require('../../config/data/categories');
const clothing_signs = require('../../config/data/clothing_signs');
const family_members = require('../../config/data/family_members');
const feelings_and_emotions = require('../../config/data/feelings_and_emotions');
const games_and_activities = require('../../config/data/games_and_activities');
const good_manners = require('../../config/data/good_manners');
const good_grooming = require('../../config/data/good_grooming');
const outdoor_signs = require('../../config/data/outdoor_signs');
const months = require('../../config/data/months');
const days = require('../../config/data/days');
const snack_signs = require('../../config/data/snack_signs');

const KEYS_TO_FILTERS = ['en', 'ph'];
const uuidv1 = require('uuid/v1');
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

const ITEMS_PER_PAGE = 8;
const sentenceCase = require('sentence-case');

export default class Dictionary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
            menuItems: [],
            // TODO: Transfer to separate file
            favoritesVisible: false
        }
    }

    searchUpdated(term) {
        this.setState({ searchTerm: term });
    }

    _keyExtractor = () => uuidv1();

    componentDidMount(){
        const data = []
        animal_signs.data.map(i=>{
            data.push(i)
        })
        basic_questions.data.map(i=>{
            data.push(i)
        })
        beginning_signs.data.map(i=>{
            data.push(i)
        })
        behavior_and_safety.data.map(i=>{
            data.push(i)
        })
        body_parts.data.map(i=>{
            data.push(i)
        })
        clothing_signs.data.map(i=>{
            data.push(i)
        })
        family_members.data.map(i=>{
            data.push(i)
        })
        feelings_and_emotions.data.map(i=>{
            data.push(i)
        })
        games_and_activities.data.map(i=>{
            data.push(i)
        })
        good_manners.data.map(i=>{
            data.push(i)
        })
        good_grooming.data.map(i=>{
            data.push(i)
        })
        outdoor_signs.data.map(i=>{
            data.push(i)
        })
        snack_signs.data.map(i=>{
            data.push(i)
        })
        months.data.map(i=>{
            data.push(i)
        })
        days.data.map(i=>{
            data.push(i)
        })

        this.setState({menuItems: data})
    }

    render() {
        const { searchTerm, menuItems, menuLogoVisible, favoritesVisible } = this.state;
        const data = menuItems.filter(createFilter(searchTerm, KEYS_TO_FILTERS))
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderColor: 'rgba(0,0,0,0.2)', paddingTop: 3, paddingBottom: 3 }}>
                    <TouchableOpacity style={[{ marginLeft: 15 }]} onPress={() => Actions.pop()}>
                        <Icon name="arrow-left" style={[styles.navIconDefault, { color: colors.primary }]} />
                    </TouchableOpacity>
                    <View style={{ flex: 1 }}>
                        <SearchInput
                            onChangeText={(term) => { this.searchUpdated(term) }}
                            style={styles.searchInput}
                            placeholder="Search a Tagalog Word"
                            clearIcon={<Icon name="x" style={{ color: colors.primary, fontSize: 20 }} />}
                            clearIconViewStyles={{ position: 'absolute', top: 15, right: 22 }}
                        />
                    </View>
                </View>
                <StatusBar backgroundColor={colors.primaryDarker} />
                <FlatList
                    data={data.slice(0,ITEMS_PER_PAGE)}
                    style={{ marginBottom: 5 }}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ paddingLeft: 0, paddingTop: 15, paddingBottom: 15, flexDirection: 'row', alignItems: 'center' }} onPress={() => Actions.meaning({title:sentenceCase(item.ph), data:item})}>
                            <Icon name="book" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                            <Text numberOfLines={1} style={{ fontSize: 15, flex:1 }}>{sentenceCase(item.ph)}</Text>
                        </TouchableOpacity>
                    )}
                    ListHeaderComponent={()=><View>                     
                        <Text style={{padding: 20, paddingBottom: 10, opacity: 0.5}}>Suggestions</Text>
                    </View>}
                    ItemSeparatorComponent={() =>
                        <View
                            style={{
                                height: 1,
                                width: '100%',
                                backgroundColor: '#F0EEE8',
                            }}
                        />
                    }
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}
