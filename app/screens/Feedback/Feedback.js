import _ from 'lodash';
import React, { Component } from 'react';
import { ScrollView, Text, TextInput, View } from 'react-native';
import RNFirebase from 'react-native-firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import prompt from 'react-native-prompt-android';
import { ActionConst, Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

const firebase = RNFirebase.app();

const feedbackRef = firebase.database().ref('feedback')

export default class Feedback extends Component {

    constructor(props) {
        super(props)
        this.state = {
            afsl: 0,
            dictionary: 0,
            vids: 0,
            translator: 0,
            fslcat: 0,
            minigame: 0,
            overall: 0,
            comment: '',
            email: ''
        }
    }

    StarComponent = props => (
        <View key={props.target}>
            <Text style={{ marginTop: 20 }}>{props.text}</Text>
            <View style={{height: 37}}>
            <StarRating
                containerStyle={{ paddingLeft: '10%', paddingRight: '10%' }}
                starSize={35}
                disabled={false}
                maxStars={5}
                rating={props.value}
                fullStarColor={colors.amber['700']}
                emptyStarColor={colors.green['700']}
                selectedStar={(rating) => this.onStarRatingPress(props.target, rating)}
            />
            </View>
        </View>
    )

    askEmail() {
        const app = this
        prompt(
            'FSL Feedback',
            'Please enter your email.',
            [
                { text: 'Cancel', onPress: () => Actions.home({type: ActionConst.REPLACE }) },
                {
                    text: 'OK', onPress: val => {
                        app.setState({email:val})
                    }
                },
            ],
            {
                type: 'text',
                cancelable: false,
                defaultValue: '',
                placeholder: ''
            }
        );
    }

    submitFeedback(){
        const { afsl, dictionary, vids, translator, fslcat, minigame, overall, comment, email } = this.state
        const key = _.snakeCase(email.split("@")[0])
        feedbackRef.child(key).update({
            afsl,
            dictionary,
            vids,
            translator,
            fslcat,
            minigame,
            overall,
            comment,
            email,
        }).then(()=>{
            alert("Feedback sent!")
        })
    }

    onStarRatingPress(name, rating) {
        this.setState({
            [name]: rating
        });
    }

    componentDidMount(){
        this.askEmail()
    }


    render() {
        const { afsl, dictionary, vids, translator, fslcat, minigame, overall, comment } = this.state
        return (
            <KeyboardAwareScrollView>
            <ScrollView style={{flex:1}} contentContainerStyle={{ flex: 1, backgroundColor: '#FFF', padding: 20 }}>
                <View>

                <Text style={{ fontSize: 20 }}>Help us improve our FSL app!</Text>
                <View>
                    {this.StarComponent({
                        text: "Animated FSL",
                        value: afsl,
                        target: "afsl"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "Categories",
                        value: dictionary,
                        target: "fslcat"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "Dictionary",
                        value: dictionary,
                        target: "dictionary"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "Video Tutorials",
                        value: vids,
                        target: "vids"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "Translator",
                        value: translator,
                        target: "translator"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "Mini Games",
                        value: minigame,
                        target: "minigame"
                    })}
                </View>
                <View>
                    {this.StarComponent({
                        text: "App Overall",
                        value: overall,
                        target: "overall"
                    })}
                </View>
                <Text style={{ marginTop: 20 }}>Comments</Text>
                <TextInput
                    multiline={true}
                    numberOfLines={4}
                    value={comment}
                    style={{marginBottom: 20}}
                    onChangeText={comment => this.setState({comment})}
                />
                </View>
                <Icon.Button style={{ padding: 20, alignItems: 'center'}} color="#fff" name="send" backgroundColor={colors.primary} onPress={()=>this.submitFeedback()}>
                            Submit
                        </Icon.Button>
            </ScrollView>
            </KeyboardAwareScrollView>
        )
    }
}