import React, { Component } from 'react';
import { View } from 'react-native';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

export default class CategoryContent extends Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={{flex:1}}>
                {this.props.content}
            </View>
        )
    }
}