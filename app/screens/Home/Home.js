import { Body, Container, Content, Header, Left, Right, Root } from 'native-base';
import React, { Component } from 'react';
import { Alert, FlatList, Image, ImageBackground, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Icon from 'react-native-vector-icons/Feather';
const animal_signs = require('../../config/data/animal_signs');
const basic_questions = require('../../config/data/basic_questions');
const beginning_signs = require('../../config/data/beginning_signs');
const body_parts = require('../../config/data/body_parts');
const behavior_and_safety = require('../../config/data/behavior_and_safety');
const categories = require('../../config/data/categories');
const clothing_signs = require('../../config/data/clothing_signs');
const family_members = require('../../config/data/family_members');
const feelings_and_emotions = require('../../config/data/feelings_and_emotions');
const games_and_activities = require('../../config/data/games_and_activities');
const good_manners = require('../../config/data/good_manners');
const good_grooming = require('../../config/data/good_grooming');
const outdoor_signs = require('../../config/data/outdoor_signs');
const months = require('../../config/data/months');
const days = require('../../config/data/days');
const snack_signs = require('../../config/data/snack_signs');
const KEYS_TO_FILTERS = ['title', 'description'];
const uuidv1 = require('uuid/v1');
const sentenceCase = require('sentence-case');
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const dictionary  = require('../../config/data/dictionary')
const date = new Date()
const did_you_know = require('../../config/data/did_you_know').data
const shuffle = require('knuth-shuffle').knuthShuffle
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
            menuLogoVisible: true,
            menuItems: []
        }
    }

    searchUpdated(term) {
        this.setState({ searchTerm: term })
    }

    componentDidMount(){
        Alert.alert("Did you know?", shuffle(did_you_know)[0].text)
        const data = []
        animal_signs.data.map(i => {
            data.push(i)
        })
        basic_questions.data.map(i => {
            data.push(i)
        })
        beginning_signs.data.map(i => {
            data.push(i)
        })
        behavior_and_safety.data.map(i => {
            data.push(i)
        })
        body_parts.data.map(i => {
            data.push(i)
        })
        clothing_signs.data.map(i => {
            data.push(i)
        })
        family_members.data.map(i => {
            data.push(i)
        })
        feelings_and_emotions.data.map(i => {
            data.push(i)
        })
        games_and_activities.data.map(i => {
            data.push(i)
        })
        good_manners.data.map(i => {
            data.push(i)
        })
        good_grooming.data.map(i => {
            data.push(i)
        })
        outdoor_signs.data.map(i => {
            data.push(i)
        })
        snack_signs.data.map(i => {
            data.push(i)
        })
        months.data.map(i => {
            data.push(i)
        })
        days.data.map(i => {
            data.push(i)
        })
        const fsl_today = data[date.getDay()]
        this.setState({menuItems:[

            {
                id: uuidv1(),
                icon: 'calendar',
                title: 'FSL of the Day',
                description: sentenceCase(fsl_today.ph) +" - "+ sentenceCase(fsl_today.en),
                action: ()=>Actions.meaning({title:sentenceCase(fsl_today.ph), data:fsl_today})
            }, 
            {
                id: uuidv1(),
                icon: 'book',
                title: 'Dictionary',
                description: 'Open the FSL dictionary.',
                action: ()=>Actions.dictionary()
            }, {
                id: uuidv1(),
                icon: 'list',
                title: 'Categories',
                description: 'Quickly get started with FSL.',
                action: ()=>Actions.categories()
            }, {
                id: uuidv1(),
                icon: 'star',
                title: 'Extras',
                description: 'Translator, games, video tutorials...etc.',
                action: ()=>Actions.extras()
            }, {
                id: uuidv1(),
                icon: 'message-circle',
                title: 'Feedback/Comments',
                description: 'Help us improve our app. Send feedback!',
                action: ()=>Actions.feedback()
            }, {
                id: uuidv1(),
                icon: 'help-circle',
                title: 'About',
                description: 'Learn more about our app.',
                action: ()=>Actions.about()
            }
        ]})
    }
    _keyExtractor = () => uuidv1();

    render() {
        const { searchTerm, menuItems, menuLogoVisible } = this.state;
        const data = menuItems.filter(createFilter(searchTerm, KEYS_TO_FILTERS))
        return (
            <Root>
                 {/* <View style={{ padding: 20 }}>
                        <SearchInput
                            onChangeText={(term) => { this.searchUpdated(term) }}
                            style={styles.searchInput}
                            placeholder="Search Menu"
                        />
                    </View> */}
                <Container style={{ flex: 1, backgroundColor: '#FFF' }}>
                    <Header style={{ backgroundColor: colors.primary }} androidStatusBarColor={colors.primary}>
                        <Left>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon style={styles.navIconDefault} name='menu' />
                            </TouchableOpacity>
                        </Left>
                        <Body>
                            <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
                                <Text style={[styles.navTitle, { marginLeft: 0 }]} numberOfLines={1}>
                                    Home
                                </Text>
                            </View>
                        </Body>
                        <Right>
                            <TouchableOpacity onPress={() => Actions.search()}>
                                <Icon style={styles.navIconDefault} name='search' />
                            </TouchableOpacity>
                        </Right>
                    </Header>
                    <Content>
                    <StatusBar backgroundColor={colors.primaryDarker} />
                      <View>
                        <ImageBackground style={styles.homeHeaderImage} source={require('../../assets/welcome-bg.png')}>
                            <View style={{ alignItems: 'center' }}>
                                {menuLogoVisible && <Image style={{ height: 50, width: 200 }} source={require('../../assets/app-text-logo.png')} resizeMode="contain" />}
                            </View>
                        </ImageBackground>
                      </View>
                <FlatList
                    data={data}
                    style={{ marginBottom: 5 }}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={[styles.menuItem, {backgroundColor: parseInt(item.id)%2===0?'rgba(0,0,0,0.03)':'#FFF'}]} onPress={() => item.action()}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name={item.icon} style={{ fontSize: 40, marginRight: 10, marginLeft: 5, color: colors.primary, fontWeight: 200 }} />
                                <View style={{ flexDirection: 'column', flex:1 }}>
                                    <Text style={{ fontSize: 17, color: colors.primaryDarker }}>{item.title}</Text>
                                    <Text style={{ marginRight: 20, opacity: 0.8 }}>{item.description}</Text>
                                </View>
                                <Icon name="chevron-right" style={{ fontSize: 25, marginRight: 0, marginLeft: 5, color: colors.primary }} />
                            </View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={this._keyExtractor}
                />
                    </Content>
                </Container>
            </Root>

        )
    }
}
