import React, { Component } from 'react';
import { ActivityIndicator, AsyncStorage, Image, ImageBackground, ScrollView, StatusBar, Text, View } from 'react-native';
import RNFirebase from 'react-native-firebase';
import prompt from 'react-native-prompt-android';
import { ActionConst, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const FBSDK = require('react-native-fbsdk');
const { LoginButton, ShareDialog, LoginManager, AccessToken } = FBSDK;
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
// const firebase = RNFirebase.app();
const settings = require('../../config/settings.js');
const firebase = RNFirebase.app();

export default class Minigame extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading:false
    }
  }

  loginWithFacebook() {
    const app = this;
    LoginManager
      .logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
          return Promise.reject(new Error('The user cancelled the request'));
        }

        console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
        // get the access token
        return AccessToken.getCurrentAccessToken();
      })
      .then(data => {
        // create a new firebase credential with the token
        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

        // login with credential
        app.setState({ loading: true })
        return firebase.auth().signInWithCredential(credential);
      })
      .then((currentUser) => {
        console.log(currentUser);
        AsyncStorage.setItem('@MySuperStore:deviceuser', JSON.stringify({playername: currentUser.displayName})).then(()=>{
          Actions.startgame({type: ActionConst.REPLACE})
        })
      })
      .catch((error) => {
        console.log(`Login fail with error: ${error}`);
      });
  }

  loginAnonymously() {
    const app = this;
    app.setState({ loading: true });
    firebase.auth().signInAnonymously().catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage);
      // ...
    });
  }

  grantOfflinePlayer(name){    
    AsyncStorage.setItem('@MySuperStore:deviceuser', JSON.stringify({playername: name}))
    Actions.startgame({type: ActionConst.REPLACE})
  }

  loginAnonymouslyOffline() {
    prompt(
      'FSL Offline Player',
      'Please enter your player name.',
      [
       {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
       {text: 'OK', onPress: val => {
         console.log(val)
        this.grantOfflinePlayer(val)
       }},
      ],
      {
          type: 'text',
          cancelable: false,
          defaultValue: '',
          placeholder: ''
      }
  );    
  }

  render() {
    const {loading} = this.state;
    return (
      <ScrollView contentContainerStyle={styles.welcomeContainer}>
      <StatusBar backgroundColor={colors.primaryDarker} />
        <ImageBackground style={{ flex: 1, alignItems: 'center', alignContent: 'center', flexDirection: 'row' }} source={require('../../assets/welcome-bg.png')}>
          <View style={{flexDirection: 'column', alignItems: 'center', flex:1}}>
          <Image style={styles.welcomeLogo} source={require('../../assets/app-logo.png')} resizeMode="contain" />
          <Text style={styles.welcomeAppTitle}>FSL</Text>
          <Text style={{ color: '#FFF', fontSize: 20}}>Mini Games</Text>
          {/* <Text style={[{ fontSize: 20 }, styles.textWhite]}>Filipino Sign Language</Text> */}
          {/* <Text style={[styles.textWhite]}>Login to start playing.</Text> */}
          {/* <Text style={[styles.textWhite]}> for Filipino Sign Language</Text>          */}
          <View style={{margin:20}}>
          <Icon.Button style={{ padding: 10}} name="facebook" backgroundColor="#3b5998" onPress={() => this.loginWithFacebook()}>
            Online Player
          </Icon.Button>
            </View>
          <Icon.Button style={{ padding: 10}} name="user" color="#333" backgroundColor="#fff" onPress={() => this.loginAnonymouslyOffline()}>
            Offline Player
          </Icon.Button>  
          </View>          
        </ImageBackground>
        {loading && 
        <View style={styles.loadingActivity}>
          <ActivityIndicator size='large' />
        </View>
      }
      </ScrollView>
    )
  }
}
