module.exports = {
    data: [
        // {
        //     ph: "balat",
        //     type: "noun",
        //     en: "Birthmark (on skin) ",
        //     ph_steps: "kinukurot ang balat ng kaliwang kamay.",
        //     en_steps: "pinch skin on back of left hand.",
        //     image_symbol: require('../../assets/sign-language-symbols/beginning_signs/balat_icon.png'),
        //     audio: require('../../assets/sign-language-audio/body_parts/balat.wav'),
        // },
        // {
        //     ph: "braso",
        //     type: "noun",
        //     en: "Arm",
        //     ph_steps: "'A' na kamay dumudulas na galaw sa sa tabi ng braso.",
        //     en_steps: "'A' hand sliding along arm.",
        //     image_symbol: require('../../assets/sign-language-symbols/beginning_signs/braso_icon.png'),
        //     audio: require('../../assets/sign-language-audio/body_parts/braso.wav'),
        // },
        {
            ph: "Katawan",
            type: "noun",
            en: "Body",
            ph_steps: "Letrang 'B' na kamay nakadampi sa dibdib, pababa sa beywang.",
            en_steps: "'B' hands touch chest then down to the waist level.",
            image_symbol: require('../../assets/animated-sign-language/body_parts/katawan.gif'),
            audio: require('../../assets/sign-language-audio/body_parts/katawan.wav'),
        },
        {
            ph: "Mata",
            type: "noun",
            en: "Eye",
            ph_steps: "Ang hintuturo ay nakaturo sa mata.",
            en_steps: "Index finger points at the right.",
            image_symbol: require('../../assets/animated-sign-language/body_parts/mata.jpg'),
            audio: require('../../assets/sign-language-audio/body_parts/mata.wav'),
        },
        {
            ph: "Siko",
            type: "noun",
            en: "Elbow",
            ph_steps: "Ang hintuturong daliri nakaturo sa siko.",
            en_steps: "Index finger pointing in elbow.",
            image_symbol: require('../../assets/animated-sign-language/body_parts/siko.jpg'),
            audio: require('../../assets/sign-language-audio/body_parts/siko.wav'),
        },
        // {
        //     ph: "ulo",
        //     type: "noun",
        //     en: "Head",
        //     ph_steps: "mga dulo ng daliri na naka baluktot na letrang 'B' na kamay na nakadampi sa noo papunta sa baba.",
        //     en_steps: "fingertip of 'B' hand bent and touching the forehead moving to the chin.",
        //     image_symbol: require('../../assets/sign-language-symbols/beginning_signs/ulo_icon.png'),
        //     audio: require('../../assets/sign-language-audio/body_parts/ulo.wav'),
        // }
    ]
}