import _ from 'lodash';
import { Card, CardItem, DeckSwiper, Text } from 'native-base';
import React, { Component } from 'react';
import { Image, View } from 'react-native';

const styles = require('../../config/styles');
const colors = require('../../config/colors');
const alphabets = require('../../config/data/alphabets');
const uuidv1 = require('uuid/v1');
export default class Alphabets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: alphabets.data
        }
    }
    _keyExtractor = () => uuidv1();
    componentDidMount() {
        console.log(this.state.data)
    }
    render() {
        const { data } = this.state;
        return (
            <View>
                <DeckSwiper
                    dataSource={data}
                    looping
                    renderItem={item => (
                        <View style={{ padding: 20, marginTop: 30 }}>
                            <Card style={{ elevation: 3 }}>
                                <CardItem />
                                <CardItem cardBody style={{ padding: 20 }}>
                                    <Image style={{ height: 300, flex: 1 }} resizeMode="contain" source={item.image} />
                                </CardItem>
                                <CardItem>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ textAlign: 'center', fontSize: 50, fontWeight: 'bold', color: colors.primary }}>
                                            {_.startCase(item.text) + _.lowerCase(item.text)}
                                        </Text>
                                    </View>
                                </CardItem>
                            </Card>
                        </View>
                    )}
                />
                <Text style={{padding: 20, fontSize: 15, opacity: 0.5}}>Swipe left or right to view the letters.</Text>
            </View>
        )
    }
}