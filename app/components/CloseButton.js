import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../config/styles');

export default class CloseButton extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={[{ marginLeft: 15 }, this.props.style]} onPress={() => Actions.pop()}>
                <Icon name="x" style={styles.navIconDefault} />
            </TouchableOpacity>
        )
    }
}