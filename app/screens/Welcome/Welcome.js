import React, { Component } from 'react';
import { Image, ImageBackground, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { ActionConst, Actions } from 'react-native-router-flux';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

export default class Welcome extends Component {
    render(){
        return (
            <View style={styles.welcomeContainer}>
            <ImageBackground style={{flex:1,alignItems: 'center',alignContent: 'center',}} source={require('../../assets/welcome-bg.png')}>
            <StatusBar backgroundColor={colors.primaryDarker} />
                <Image style={styles.welcomeLogo} source={require('../../assets/app-logo.png')} resizeMode="contain"/>                
                <Text style={styles.welcomeAppTitle}>FSL</Text>
                <Text style={[{fontSize: 20},styles.textWhite]}>Filipino Sign Language</Text>
                <Text style={[styles.textWhite]}>Offline Mobile e-Learning</Text>
                <TouchableOpacity style={styles.welcomeStartButton} onPress={()=>Actions.home({type: ActionConst.REPLACE})}><Text style={{fontSize:15}}>GET STARTED</Text></TouchableOpacity>
            </ImageBackground>
            </View>
        )
    }
}