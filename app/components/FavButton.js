import React, { Component } from 'react';
import { AsyncStorage, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = require('../config/styles');

export default class FavButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toggled: false,
        }
        this.toggleFav = this.toggleFav.bind(this)
    }

    toggleFav(){
        
        this.setState({toggled:!this.state.toggled})
        const data = this.props.data
        let oldData = {}
        
        AsyncStorage.getItem('@MySuperStore:favorites').then(value=>{
            const existingData = JSON.stringify(JSON.parse(value))!=='null'?JSON.parse(value):[]
            // const combined = Object.assign(JSON.parse(value), data)
            console.log(existingData)
            if(this.state.toggled){
                existingData.push(data)
                AsyncStorage.setItem('@MySuperStore:favorites', JSON.stringify(existingData))
            }
            console.log(existingData)
        }).catch(err=>{
            console.log(err)
        })

      

    }

    componentDidMount(){
        console.log(this.props.data)
    }

    render() {
        const {toggled} = this.state;
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {/* <TouchableOpacity style={[{ marginRight: 15 }, this.props.style]}>
                    <Icon name="video" style={styles.navIconDefault} />
                </TouchableOpacity> */}
                <TouchableOpacity style={[{ marginRight: 15 }, this.props.style]} onPress={()=>this.toggleFav()}>
                    <Icon name={toggled?"heart":"heart-o"} style={styles.navIconDefault} />
                </TouchableOpacity>
            </View>

        )
    }
}