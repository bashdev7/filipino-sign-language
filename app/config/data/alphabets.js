const uuidv1 = require('uuid/v1');
module.exports = {
    data: [
        {
            id: uuidv1(),
            text: "A",
            image: require('../../assets/animated-sign-language/alphabets/a.jpg')
        },
        {
            id: uuidv1(),
            text: "B",
            image: require('../../assets/animated-sign-language/alphabets/b.jpg')
        },
        {
            id: uuidv1(),
            text: "C",
            image: require('../../assets/animated-sign-language/alphabets/c.jpg')
        },
        {
            id: uuidv1(),
            text: "D",
            image: require('../../assets/animated-sign-language/alphabets/d.jpg')
        },
        {
            id: uuidv1(),
            text: "E",
            image: require('../../assets/animated-sign-language/alphabets/e.jpg')
        },
        {
            id: uuidv1(),
            text: "F",
            image: require('../../assets/animated-sign-language/alphabets/f.jpg')
        },
        {
            id: uuidv1(),
            text: "G",
            image: require('../../assets/animated-sign-language/alphabets/g.jpg')
        },
        {
            id: uuidv1(),
            text: "H",
            image: require('../../assets/animated-sign-language/alphabets/h.jpg')
        },
        {
            id: uuidv1(),
            text: "I",
            image: require('../../assets/animated-sign-language/alphabets/i.jpg')
        },
        {
            id: uuidv1(),
            text: "J",
            image: require('../../assets/animated-sign-language/alphabets/j.gif')
        },
        {
            id: uuidv1(),
            text: "K",
            image: require('../../assets/animated-sign-language/alphabets/k.jpg')
        },
        {
            id: uuidv1(),
            text: "L",
            image: require('../../assets/animated-sign-language/alphabets/l.jpg')
        },
        {
            id: uuidv1(),
            text: "M",
            image: require('../../assets/animated-sign-language/alphabets/m.jpg')
        },
        {
            id: uuidv1(),
            text: "N",
            image: require('../../assets/animated-sign-language/alphabets/n.jpg')
        },
        {
            id: uuidv1(),
            text: "O",
            image: require('../../assets/animated-sign-language/alphabets/o.jpg')
        },
        {
            id: uuidv1(),
            text: "P",
            image: require('../../assets/animated-sign-language/alphabets/p.jpg')
        },
        {
            id: uuidv1(),
            text: "Q",
            image: require('../../assets/animated-sign-language/alphabets/q.jpg')
        },
        {
            id: uuidv1(),
            text: "R",
            image: require('../../assets/animated-sign-language/alphabets/r.jpg')
        },
        {
            id: uuidv1(),
            text: "S",
            image: require('../../assets/animated-sign-language/alphabets/s.jpg')
        },
        {
            id: uuidv1(),
            text: "T",
            image: require('../../assets/animated-sign-language/alphabets/t.jpg')
        },
        {
            id: uuidv1(),
            text: "U",
            image: require('../../assets/animated-sign-language/alphabets/u.jpg')
        },
        {
            id: uuidv1(),
            text: "V",
            image: require('../../assets/animated-sign-language/alphabets/v.jpg')
        },
        {
            id: uuidv1(),
            text: "W",
            image: require('../../assets/animated-sign-language/alphabets/w.jpg')
        },
        {
            id: uuidv1(),
            text: "X",
            image: require('../../assets/animated-sign-language/alphabets/x.jpg')
        },
        {
            id: uuidv1(),
            text: "Y",
            image: require('../../assets/animated-sign-language/alphabets/y.jpg')
        },
        {
            id: uuidv1(),
            text: "Z",
            image: require('../../assets/animated-sign-language/alphabets/z.gif')
        },
    ]
}