import React, { Component } from 'react';
import { Dimensions, Image, ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import RNFirebase from 'react-native-firebase';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const FBSDK = require('react-native-fbsdk');
const Sound = require('react-native-sound');
const { LoginButton, ShareDialog, LoginManager, AccessToken } = FBSDK;
const data = require('../../config/data/dictionary');
const shuffle = require('knuth-shuffle').knuthShuffle;
const sentenceCase = require('sentence-case')
const uuidv4 = require('uuid/v4')
const width = Dimensions.get('window').width; //full width
const colors = require('../../config/colors')
const settings = require('../../config/settings.js');
const firebase = RNFirebase.initializeApp(settings.firebase);

Sound.setCategory('Playback');

export default class MiniGame extends Component {

    constructor(props) {
        super(props)
        this.state = {
            questions: [],
            index: 0,
            loggedin: false,
            loading: false,
            isPlaying: false
        }
        this.sound = null
    }

    toggleSound(){
        if(this.state.isPlaying){
            this.stopSound();
            this.setState({isPlaying: !this.state.isPlaying})
        }else{
            this.playSound();
            this.setState({isPlaying: !this.state.isPlaying})
        }
    }

    playSound() {
        this.sound.setVolume(0.1);
        this.sound.play((success) => {
            // this.sound.release();
            if (success) {
              console.log('successfully finished playing');
              this.sound.stop();
              this.setState({isPlaying: !this.state.isPlaying})
            } else {
              console.log('playback failed due to audio decoding errors');
            }
          });
    }

    stopSound(){
        this.sound.stop()
    }

    componentWillMount() {
        this.sound = new Sound(require('../../assets/music.mp3'), Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + test.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        });

        this.playSound()
    }

    componentDidMount() {
        this.playSound()
    }

    render() {
        const { loggedin, loading } = this.state
        return (
            <ScrollView style={{ backgroundColor: colors.primary }} contentContainerStyle={{flex:1, flexGrow:1}}>
                <ImageBackground source={require('../../assets/minigame.jpg')} style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/app-logo.png')} style={{ width: 100, height: 100 }} />
                    <Text style={{ color: '#FFF', fontSize: 40 }}>FSL</Text>
                    <Text style={{ color: '#FFF', fontSize: 20, marginBottom: 30 }}>Mini Games</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{ margin: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 35, borderColor: '#FFF', borderWidth: 5 }} onPress={()=>Actions.leaderboard()}>
                                <Icon name="bar-chart" style={{ color: '#FFF', fontSize: 25 }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ margin: 10, width: 70, height: 70, justifyContent: 'center', alignItems: 'center', borderRadius: 35, borderColor: '#FFF', borderWidth: 5 }} onPress={()=>Actions.startgamecategories()}>
                                <Icon name="play" style={{ color: '#FFF', fontSize: 35 }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ margin: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 35, borderColor: '#FFF', borderWidth: 5 }} onPress={()=>this.toggleSound()}>
                                <Icon name="volume-2" style={{ color: '#FFF', fontSize: 25 }} />
                            </TouchableOpacity>
                        </View>
                </ImageBackground>
            </ScrollView>
        )
    }
}