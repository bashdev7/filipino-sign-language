import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import History from '../../components/ScreenCategories/History';

const animal_signs = require('./animal_signs');
const basic_questions = require('./basic_questions');
const beginning_signs = require('./beginning_signs');
const behavior_and_safety = require('./behavior_and_safety');
const categories = require('./categories');
const clothing_signs = require('./clothing_signs');
const family_members = require('./family_members');
const feelings_and_emotions = require('./feelings_and_emotions');
const games_and_activities = require('./games_and_activities');
const good_manners = require('./good_manners');
const good_grooming = require('./good_grooming');
const outdoor_signs = require('./outdoor_signs');
const snack_signs = require('./snack_signs');

const uuidv1 = require('uuid/v1');
const colors = require('../../config/colors.js');
const sentenceCase = require('sentence-case');

module.exports = {
    data: [
        {
            id: uuidv1(),
            title: 'FSL History',
            action: ()=>Actions.category_content({content: <History />, title: 'FSL History'}),
            color: colors.brown.a700,
            desc: "Learn how FSL started."
        }, 
        {
            id: uuidv1(),
            title: 'Translator',
            action: ()=>Actions.translator({content: "x", title: 'Translator'}),
            color: colors.blue.a700,
            desc: "Quickly find the Filipino term meaning and it's quivalent FSL."
        }, 
        {
            id: uuidv1(),
            title: 'Video Tutorials',
            action: ()=>Actions.videos(),
            color: colors.red.a400,
            desc: "Quick video tutorials to get started."
        },
        {
            id: uuidv1(),
            title: 'Mini Game',
            action: ()=> Actions.minigame(),
            color: colors.orange.a400,
            desc: "Wan't to test your skills? Play our mini games!"
        }
    ]
}