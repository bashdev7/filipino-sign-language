import React, { Component } from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const uuidv4 = require('uuid/v4')

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');


const persons = [
    'Dr. Ramil G. Lumauag',
    'Sir Anrem J. Balontong',
    'Dr. Rodel D. Dosano',
    'Sir Ned Cababasay',
    "Ma'am Tracy N. Tacuban",
    "Ma'am May Florence J. Franco",
    'Mark Joseph J. Solidarios',
    'Raffy Costorio',
    'Sir Raymond J. Manding',
    'Ms. Kat Leandicho',
    'Ms. Charmaine D. Arcelon',
    'Mr. Aron John Borbon',
    'Ms. Shela Joy Bolivar',
    'Mr. Jefferson Clamor',
    'Mr. Jerald John Layam',
    'WIT IT Department, Students, Faculty & Staff',
    'My Family, My Wife and Kids',
    'To God be the Glory!'
]
export default class About extends Component {
    render() {
        return (
            <KeyboardAwareScrollView>
                <ScrollView contentContainerStyle={{ flex: 1, padding: 20, backgroundColor: colors.white }}>
                    <View>
                        <Image source={require('../../assets/app-logo.png')} style={{ width: 50, marginBottom: 20, height: 50 }} resizeMode="contain" />
                        <Text style={{ marginBottom: 20, fontWeight: 'bold', textAlign:'center', fontSize: 17, marginBottom: 20 }}>An Offline Learners Aid for Filipino Sign Language in Android System</Text>
                        <Text style={{ marginBottom: 20 }}>Persons with and without speech and hearing impairment can now have access to the Filipino Sign Language without going to the school for PWD.</Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold', textAlign: 'center' }}>Special Thanks</Text>
                        {
                            persons.map(i=>(
                                <Text key={uuidv4()} style={{ marginBottom: 10, textAlign: 'center' }}>{i}</Text>
                            ))
                        }                        
                        <Text style={{ marginBottom: 10, marginTop: 10, fontWeight: 'bold', textAlign: 'center' }}>Privacy Policy</Text>
                        <Text style={{ marginBottom: 10 }}>Arnold Narte built the Filipino Sign Language app as a Free app. This SERVICE is provided by Arnold Narte at no cost and is intended for use as is. </Text>
                        <Text style={{ marginBottom: 10 }}>This page is used to inform website visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service. </Text>
                        <Text style={{ marginBottom: 10 }}>If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy. </Text>
                        <Text style={{ marginBottom: 10 }}>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Filipino Sign Language unless otherwise defined in this Privacy Policy. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Information Collection and Use</Text>
                        <Text style={{ marginBottom: 10 }}>For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request is retained on your device and is not collected by me in any way </Text>
                        <Text style={{ marginBottom: 10 }}>The app does use third party services that may collect information used to identify you.</Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Log Data</Text>
                        <Text style={{ marginBottom: 10 }}>I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Service Providers</Text>
                        <Text style={{ marginBottom: 10 }}>I may employ third-party companies and individuals due to the following reasons: To facilitate our Service; To provide the Service on our behalf; To perform Service-related services; or To assist us in analyzing how our Service is used.</Text>
                        <Text style={{ marginBottom: 10 }}>I want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Security</Text>
                        <Text style={{ marginBottom: 10 }}>I value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee its absolute security. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Links to Other Sites</Text>
                        <Text style={{ marginBottom: 10 }}>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Children's Privacy</Text>
                        <Text style={{ marginBottom: 10 }}>These Services do not address anyone under the age of 13. I do not knowingly collect personally identifiable information from children under 13. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Changes to This Privacy Policy</Text>
                        <Text style={{ marginBottom: 10 }}>I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Children's Privacy</Text>
                        <Text style={{ marginBottom: 10 }}>If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me arn.narte@gmail.com. </Text>
                        <Text style={{ marginBottom: 10, fontWeight: 'bold', textAlign: 'center' }}>Arnold M. Narte (arn.narte@gmail.com)</Text>
                    </View>
                </ScrollView>
            </KeyboardAwareScrollView>
        )
    }
}