import { Card, CardItem, DeckSwiper, Text } from 'native-base';
import React, { Component } from 'react';
import { Alert, Image, View } from 'react-native';
import CountDown from 'react-native-countdown-component';

const shuffle = require('knuth-shuffle').knuthShuffle;
const styles = require('../../config/styles');
const colors = require('../../config/colors');
const alphabets = require('../../config/data/alphabets');
const dictionary = require('../../config/data/dictionary').words_phrases
const questionBucket = shuffle(dictionary).splice(0, 30)
const uuidv1 = require('uuid/v1');

export default class Panic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: questionBucket
        }
    }
    _keyExtractor = () => uuidv1();
    componentDidMount() {
        Alert.alert('FSL Panic Demo Mode', 'Swipe left if wrong, swipe right otherwise.')
    }

    endGame() {
        alert("Game Finished!")
    }
    randomNumber = () => Math.abs(Math.floor(Math.random() * questionBucket.length) - 1)
    render() {
        const { data } = this.state;
        return (
            <View>
                <DeckSwiper
                    dataSource={data}
                    looping
                    renderItem={item => (
                        <View style={{ padding: 20, marginTop: 50 }}>
                            <Card style={{ elevation: 3 }}>
                                <CardItem>
                                    <Text numberOfLines={1} style={{ textAlign: 'center', fontSize: 30, fontWeight: 'bold', color: colors.primary }}>
                                        {questionBucket[this.randomNumber()].en}
                                    </Text>
                                </CardItem>
                                <CardItem cardBody style={{ padding: 20 }}>
                                    <Image style={{ height: 300, flex: 1 }} resizeMode="contain" source={item.image_symbol} />
                                </CardItem>
                            </Card>
                        </View>
                    )}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 20 }}>Time left: </Text>
                    <CountDown
                        until={60}
                        onFinish={() => this.endGame()}
                        size={20}
                        timeToShow={["S"]}
                        digitTxtColor="#FFF"
                        digitBgColor={colors.amber['700']}
                        style={{ marginTop: 10 }}
                        timeTxtColor="#FFF"
                    />
                </View>
            </View>
        )
    }
}