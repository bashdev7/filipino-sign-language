// missing bata
module.exports = {
    data: [
        {
        ph: "Ate",
        type: "noun",
        en: "Appellation for elder sister, used with or without the given name (term of address reference)",
        ph_steps: "Isang kamay ay nasa senyas na letarng 'L' na ang hinlalaki ay nakadampi sa baba pagkatapos igalaw pababa para magkita ang isa mo pang kamay na naka letrang 'L'.",
        en_steps: "One hand form an 'L' with the thumb touching the chin, it then moves downward to meet the other 'L' shaped hand.",
        image_symbol: require('../../assets/animated-sign-language/family/ate.gif'),
        audio: require('../../assets/sign-language-audio/family_members/ate.wav'),
    },
    // {
    //     ph: "sanggol",
    //     type: "noun",
    //     en: "baby",
    //     ph_steps: "ang isa braso ay inuugoy ang isa pang braso.",
    //     en_steps: "one arm cradles the other and bith move side to side in a rocking motion.",
    //     image_symbol: require('../../assets/sign-language-symbols/default.png'),
    //     audio: require('../../assets/sign-language-audio/family_members/ate.wav'), //fixme
    // },
    {
        ph: "Kuya",
        type: "noun",
        en: "Older Brother (term of address, reference)",
        ph_steps: "Isang kamay ay nasa senyas na letrang 'L' saka ang hinlalaki ay nakadampi sa iyong noo, tapos igalaw pababa para magkita ang isa mo pang kamay na naka-letrang-l.",
        en_steps: "One hand forms an 'L' with the thumb touching the forehead, it then moves downward to meet the other 'L' shaped hand.",
        image_symbol: require('../../assets/animated-sign-language/family/kuya.gif'),
        audio: require('../../assets/sign-language-audio/family_members/kuya.wav'),
    },
    // {
    //     ph: "lola",
    //     type: "noun",
    //     en: "grandmother (term of address and reference)",
    //     en_steps: "the thumb of your open hand taps your chin twice and then makes two arc down and away.",
    //     image_symbol: require('../../assets/sign-language-symbols/family_members/lola_icon.png'),
    //     audio: require('../../assets/sign-language-audio/family_members/lola.mp3'),
    // },
    // {
    //     ph: "lolo",
    //     type: "noun",
    //     en: "grandfather (term of address and reference)",
    //     en_steps: "the thumb of your open hand taps your forehead twice and them makes two arcs down and away.",
    //     image_symbol: require('../../assets/sign-language-symbols/family_members/lolo_icon.png'),
    //     audio: require('../../assets/sign-language-audio/family_members/lolo.mp3'),
    // },
    {
        ph: "Nanay",
        type: "noun",
        en: "Mother (term of address and reference) [syn. inay]",
        ph_steps: "Ibuka ang iyong kamay, samantalang ang iyong hinlalaki ay ilapat ng dalawang beses sa ilalim ng iyong baba.",
        en_steps: "The thumb of your open hand taps your chin twice.",
        image_symbol: require('../../assets/animated-sign-language/family/nanay.gif'),
        audio: require('../../assets/sign-language-audio/family_members/nanay.mp3'),
    },
    // {
    //     ph: "pamilya",
    //     type: "noun",
    //     en: "family",
    //     en_steps: "both hands in f moving in circular motion to palm in.",
    //     image_symbol: require('../../assets/sign-language-symbols/family_members/pamilya_icon.png'),
    //     audio: require('../../assets/sign-language-audio/family_members/pamilya.wav'),
    // },
    {
        ph: "Tatay",
        type: "noun",
        en: "Father | Daddy (term of address and reference)",
        ph_steps: "Ibuka ang iyong kamay, samantalang ang iyong hinlalaki ay ilapat sa noo ng dalawang beses.",
        en_steps: "The thumb of your open hand taps your forehead twice.",
        image_symbol: require('../../assets/animated-sign-language/family/tatay.gif'),
        audio: require('../../assets/sign-language-audio/family_members/tatay.mp3'),
    }
    ]
}
