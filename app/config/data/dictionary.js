const data = []
const animal_signs = require('./animal_signs');
const basic_questions = require('./basic_questions');
const beginning_signs = require('./beginning_signs');
const body_parts = require('./body_parts');
const behavior_and_safety = require('./behavior_and_safety');
const categories = require('./categories');
const clothing_signs = require('./clothing_signs');
const family_members = require('./family_members');
const feelings_and_emotions = require('./feelings_and_emotions');
const games_and_activities = require('./games_and_activities');
const good_manners = require('./good_manners');
const good_grooming = require('./good_grooming');
const outdoor_signs = require('./outdoor_signs');
const months = require('./months');
const days = require('./days');
const snack_signs = require('./snack_signs');



const dictionary = {
    words_phrases: data.concat(
        animal_signs,
        basic_questions,
        beginning_signs,
        behavior_and_safety,
        body_parts,
        clothing_signs,
        family_members,
        feelings_and_emotions,
        games_and_activities,
        good_manners,
        good_grooming,
        outdoor_signs,
        snack_signs,
        months,
        days,
    )
}

// console.log(data)

module.exports = dictionary



// module.exports = {
//     words_phrases: set
// }

//require('../../assets/sign-language-symbols/default.png')