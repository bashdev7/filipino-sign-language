import React, { Component } from 'react';
import { Dimensions, Image, ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import RNFirebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const FBSDK = require('react-native-fbsdk');
const Sound = require('react-native-sound');
const { LoginButton, ShareDialog, LoginManager, AccessToken } = FBSDK;
const data = require('../../config/data/dictionary');
const shuffle = require('knuth-shuffle').knuthShuffle;
const sentenceCase = require('sentence-case')
const uuidv4 = require('uuid/v4')
const width = Dimensions.get('window').width; //full width
const colors = require('../../config/colors')
const settings = require('../../config/settings.js');
const firebase = RNFirebase.app();

Sound.setCategory('Playback');

export default class MiniGameCategories extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        const { loggedin, loading } = this.state
        return (
            <ScrollView style={{ backgroundColor: colors.primary }} contentContainerStyle={{flex:1, flexGrow:1}}>
                <ImageBackground source={require('../../assets/minigame.jpg')} style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/app-logo.png')} style={{ width: 100, height: 100 }} />
                    <Text style={{ color: '#FFF', fontSize: 40 }}>FSL</Text>
                    <Text style={{ color: '#FFF', fontSize: 20, marginBottom: 30 }}>Mini Games</Text>
                    <View style={{padding: 10}}>
                        <Icon.Button style={{ padding: 10}} color="#333" name="gamepad" backgroundColor="#FFF" onPress={()=>Actions.easy()}>
                            Easy (Alphabets/Numbers)
                        </Icon.Button>
                    </View>
                    <View style={{padding: 10}}>
                        <Icon.Button style={{ padding: 10}} color="#333" name="gamepad" backgroundColor="#FFF" onPress={()=>Actions.hard()}>
                            Hard (Words/Phrases)
                        </Icon.Button>
                    </View>
                    {/* <View style={{padding: 10}}>
                        <Icon.Button style={{ padding: 10}} color="#333" name="gamepad" backgroundColor="#FFF" onPress={()=>Actions.panic()}>
                            Panic! (Demo Only)
                        </Icon.Button>
                    </View> */}
                    {/* <View style={{padding: 10}}>
                        <Icon.Button style={{ padding: 10}} color="#333" name="star" backgroundColor="#FFF" onPress={()=>Actions.easy()}> 
                            Hard
                        </Icon.Button>
                    </View> */}
                   
                </ImageBackground>
            </ScrollView>
        )
    }
}