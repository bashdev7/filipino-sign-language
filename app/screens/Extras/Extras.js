import React, { Component } from 'react';
import { FlatList, ImageBackground, Text, TouchableOpacity, View } from 'react-native';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const extras = require('../../config/data/extras');
const uuidv1 = require('uuid/v1');

export default class Extras extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: extras.data
        }
    }
    _keyExtractor = () => uuidv1();
    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5 }}>
                <FlatList
                    data={data}
                    style={{ marginBottom: 5 }}
                    renderItem={({ item }) => (
                        <View style={styles.card}>
                            <ImageBackground style={{height: 150}} source={require('../../assets/welcome-bg.png')}>
                                <View style={{backgroundColor:item.color, height: 150, position: 'absolute', top:0, bottom:0, left:0, right:0, opacity: 0.5}}></View>
                                <Text style={styles.cardTitle}>{item.title}</Text>
                            </ImageBackground>
                            <View style={styles.cardContent}>
                                <Text>{item.desc}</Text>
                            </View>
                            <View style={styles.separator}></View>
                            <TouchableOpacity style={styles.cardAction} onPress={item.action}>
                                <Text style={{fontSize: 14, color:colors.primary}}>GET STARTED</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}