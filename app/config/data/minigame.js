const data = require('../data/dictionary');
const shuffle = require('knuth-shuffle').knuthShuffle;
module.exports = shuffle(data);
// module.exports = {
//     // questions: [
//     //     {
//     //         identify: shuffledData.words_phrases[0].image_symbol,
//     //         options: [{
//     //             a: shuffledData.words_phrases[0].en,
//     //             b: shuffledData.words_phrases[1].en,
//     //             c: shuffledData.words_phrases[2].en,
//     //             d: shuffledData.words_phrases[3].en,
//     //         }],
//     //         answer: shuffledData.words_phrases[0].en
//     //     }
//     // ]
// }