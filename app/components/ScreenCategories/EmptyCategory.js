import { Text } from 'native-base';
import React, { Component } from 'react';
import { View } from 'react-native';

const styles = require('../../config/styles');
const colors = require('../../config/colors');
const alphabets = require('../../config/data/alphabets');
const uuidv1 = require('uuid/v1');
export default class EmptyCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    _keyExtractor = () => uuidv1();
    componentDidMount() {
       
    }
    render() {
        // const { data } = this.state;
        return (
            <View>
                <Text>Dataset is still empty.</Text>
            </View>
        )
    }
}