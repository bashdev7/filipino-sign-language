import React, { Component } from 'react';
import { AppState, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../config/styles');
const colors = require('../config/colors');
const Sound = require('react-native-sound');
// Enable playback in silence mode
Sound.setCategory('Playback');

// Preload sound
// var test = new Sound(require('../assets/test.mp3'), Sound.MAIN_BUNDLE, (error) => {
//     if (error) {
//         console.log('failed to load the sound', error);
//         return;
//     }
//     // loaded successfully
//     console.log('duration in seconds: ' + test.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
// });


export default class SoundButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isPlaying: false
        }
        this.sound = null
    }

    toggleSound(){
        if(this.state.isPlaying){
            this.stopSound();
            this.setState({isPlaying: !this.state.isPlaying})
        }else{
            this.playSound();
            this.setState({isPlaying: !this.state.isPlaying})
        }
    }

    playSound() {
        this.sound.play((success) => {
            // this.sound.release();
            if (success) {
              console.log('successfully finished playing');
              this.sound.stop();
              this.setState({isPlaying: !this.state.isPlaying})
            } else {
              console.log('playback failed due to audio decoding errors');
            }
          });
    }

    stopSound(){
        this.sound.stop()
    }

    componentDidMount() {
        this.sound = new Sound(this.props.src, Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + test.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        });
        AppState.addEventListener('change', (state) => {
          if(state === 'background'){
            this.sound.stop()
          }
        })
      }


componentWillUnmount(){
        this.sound.stop()
    }

    render() {
        const {isPlaying} = this.state;
        return (
            <TouchableOpacity style={[{ marginLeft: 0}, this.props.style]} onPress={() => this.toggleSound()}>
                <Icon name="volume-2" style={[styles.soundButtonIcon, {color: isPlaying?"orange":colors.primary}]} />
            </TouchableOpacity>
        )
    }
}