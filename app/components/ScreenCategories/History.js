import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import VideoPlayer from 'react-native-video-player';

// import Video from 'react-native-video';
const styles = require('../../config/styles');
const colors = require('../../config/colors');
const sentenceCase = require('sentence-case');
const uuidv1 = require('uuid/v1');

const paragraph_set_1 = [
    {
        text: 'Did you know that a Filipino who is deaf is actually using Signing Exact English? Which means they are English speaking literate?',
    },
    {
        text: 'Signing Exact English is a system of manual communication that strives to be an exact representation of English vocabulary and grammar, it is a part of American Sign Language and rooted in the family of French Sign Language.',
    },
    {
        text: 'So how do we differentiate Signing Exact English and American Sign Language?',
    },
    {
        text: 'American Sign Language is a language that has a unique grammar and vocabulary that is different from English or any other spoken language. ',
    },
    {
        text: 'Signing Exact Language is a sign communication system that uses signs and fingerspelling to code English. Signing Exact Language was taught in the Philippines during the first 1880’s where Americans established a school in Laguna teaching Filipino deaf to use SEE. In the other hand, starting 1982, the Internal Deaf Association (IDEA) led by former Peace Corps Volunteer G. Dennis Drake, established a series of residential elementary programs in Bohol using Philippine Sign Language as the primary language of instruction. The Bohol Deaf Academy also primarily emphasizes Filipino Sign Language. From then and now they are still practicing the SEE even though Filipino’s established the FSL a long time ago, because there are only few numbers of institutions and organizations that serves the community to learn the FSL. And finally, the government made move this late 2000’s to make a constitution that every Deaf Filipino should be literate in Filipino Sign Language. Actually the most recent statistic shows that there are only 54% of deaf Filipinos who are literate in FSL, and the other percentage does not even have a mother tongue language.',
    },
]

export default class History extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    _keyExtractor = () => uuidv1();
    componentDidMount(){
        //this.player.presentFullscreenPlayer()
    }
    render() {
        const {data} = this.state;
        return (
            <ScrollView style={{flex:1, backgroundColor: colors.primary}}>
            <Text style={{fontSize: 25,padding: 20, color: colors.white}}>
                    Filipino Sign Language
                </Text>
                <VideoPlayer
                    fullScreenOnLongPress={true}
                    video={require('../../assets/sign-language-videos/history.mp4')}
                    style={{flex:1, height: 200}}
                    ref={r => this.player = r}
                />
                {/* <Video
            ref={(ref) => {
                this.player = ref
              }}  
                repeat
                resizeMode='contain'
                source={require('../../assets/sign-language-videos/history.mp4')}
                style={{flex:1, height: 200}}
                /> */}
                {
                    paragraph_set_1.map(i=>(
                        <Text key={i} style={{padding: 20, paddingBottom:0, color: colors.white}}>
                            {i.text}
                        </Text>
                    ))
                }

            </ScrollView>

        )
    }
}