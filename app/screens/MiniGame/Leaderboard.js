import _ from 'lodash'
import React, { Component } from 'react'
import { Image, ScrollView, Text, View } from 'react-native'
import RNFirebase from 'react-native-firebase'
import { Tab, Tabs } from 'native-base'

const firebase = RNFirebase.app();
const leaderboardRef = firebase.database().ref('leaderboard')
const uuidv4 = require('uuid/v4')

class LeaderboardEasy extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            d: null
        }
    }
    componentDidMount() {
        const app = this
        leaderboardRef.on("value", s => {
            const res = s.val()
            const data = []
            if (JSON.stringify(res).length > 5) {
                Object.keys(res).map(i => {
                    const es = res[i].easy !== undefined ? res[i].easy : 0
                    // const ps = res[i].panic!==undefined?res[i].panic:0
                    data.push({
                        name: res[i].name,
                        easy: es,
                        // panic: ps,
                        total: parseInt(es)
                    })
                })
                app.setState({ data: _.orderBy(data, ['total'], ['desc']) })
            }
        })
    }

    componentWillUnmount() {
        leaderboardRef.off()
    }
    render() {
        const { data } = this.state
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                    <Text style={{ flex: 1 }}>Rank - Player</Text>
                    <Text>Easy</Text>
                </View>
                {data.length > 0 ? Object.keys(data).map(i => (
                    <View key={uuidv4()} style={{ flexDirection: 'row', backgroundColor: '#FFF', padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 10 }}>{parseInt(i) + 1}</Text>
                        <Image style={{ height: 50, width: 50 }} source={require('../../assets/app-logo.png')} resizeMode="contain" />
                        <Text numberOfLines={1} style={{ flex: 1, marginLeft: 20, marginRight: 10, fontSize: 20 }}>{data[i].name}</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{data[i].easy}</Text>
                        {/* <Text style={{fontSize:20}}>/</Text>
                        <Text style={{fontSize:20, fontWeight: 'bold'}}>{data[i].panic}</Text> */}
                    </View>
                ))
                    :
                    <View>
                        <Text style={{ padding: 10 }}>Loading data...</Text>
                    </View>
                }
            </ScrollView>
        );
    }
}

class LeaderboardHard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            d: null
        }
    }
    componentDidMount() {
        const app = this
        leaderboardRef.on("value", s => {
            const res = s.val()
            const data = []
            if (JSON.stringify(res).length > 5) {
                Object.keys(res).map(i => {
                    const hs = res[i].hard !== undefined ? res[i].hard : 0
                    // const ps = res[i].panic!==undefined?res[i].panic:0
                    data.push({
                        name: res[i].name,
                        hard: hs,
                        // panic: ps,
                        total: parseInt(hs)
                    })
                })
                app.setState({ data: _.orderBy(data, ['total'], ['desc']) })
            }
        })
    }

    componentWillUnmount() {
        leaderboardRef.off()
    }
    render() {
        const { data } = this.state
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                    <Text style={{ flex: 1 }}>Rank - Player</Text>
                    <Text>Hard</Text>
                </View>
                {data.length > 0 ? Object.keys(data).map(i => (
                    <View key={uuidv4()} style={{ flexDirection: 'row', backgroundColor: '#FFF', padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 10 }}>{parseInt(i) + 1}</Text>
                        <Image style={{ height: 50, width: 50 }} source={require('../../assets/app-logo.png')} resizeMode="contain" />
                        <Text numberOfLines={1} style={{ flex: 1, marginLeft: 20, marginRight: 10, fontSize: 20 }}>{data[i].name}</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{data[i].hard}</Text>
                        {/* <Text style={{fontSize:20}}>/</Text>
                        <Text style={{fontSize:20, fontWeight: 'bold'}}>{data[i].panic}</Text> */}
                    </View>
                ))
                    :
                    <View>
                        <Text style={{ padding: 10 }}>Loading data...</Text>
                    </View>
                }
            </ScrollView>
        );
    }
}

class LeaderboardAll extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            d: null
        }
    }
    componentDidMount() {
        const app = this
        leaderboardRef.on("value", s => {
            const res = s.val()
            const data = []
            if (JSON.stringify(res).length > 5) {
                Object.keys(res).map(i => {
                    const es = res[i].easy !== undefined ? res[i].easy : 0
                    const hs = res[i].hard !== undefined ? res[i].hard : 0
                    // const ps = res[i].panic!==undefined?res[i].panic:0
                    data.push({
                        name: res[i].name,
                        easy: es,
                        hard: hs,
                        // panic: ps,
                        total: parseInt(es) + parseInt(hs)
                    })
                })
                app.setState({ data: _.orderBy(data, ['total'], ['desc']) })
            }
        })
    }

    componentWillUnmount() {
        leaderboardRef.off()
    }
    render() {
        const { data } = this.state
        return (
            <ScrollView>
                <View style={{ flexDirection: 'row', padding: 5 }}>
                    <Text style={{ flex: 1 }}>Rank - Player</Text>
                    <Text>Easy/Hard</Text>
                </View>
                {data.length > 0 ? Object.keys(data).map(i => (
                    <View key={uuidv4()} style={{ flexDirection: 'row', backgroundColor: '#FFF', padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 10 }}>{parseInt(i) + 1}</Text>
                        <Image style={{ height: 50, width: 50 }} source={require('../../assets/app-logo.png')} resizeMode="contain" />
                        <Text numberOfLines={1} style={{ flex: 1, marginLeft: 20, marginRight: 10, fontSize: 20 }}>{data[i].name}</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{data[i].easy}</Text>
                        <Text style={{ fontSize: 20 }}>/</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{data[i].hard}</Text>
                        {/* <Text style={{fontSize:20}}>/</Text>
                        <Text style={{fontSize:20, fontWeight: 'bold'}}>{data[i].panic}</Text> */}
                    </View>
                ))
                    :
                    <View>
                        <Text style={{ padding: 10 }}>Loading data...</Text>
                    </View>
                }
            </ScrollView>
        );
    }
}

class Leaderboard extends Component {
    render() {
        return (
            <Tabs initialPage={0}>
                <Tab heading="Easy">
                    <LeaderboardEasy />
                </Tab>
                <Tab heading="Hard">
                    <LeaderboardHard />
                </Tab>
                <Tab heading="Overall">
                    <LeaderboardAll />
                </Tab>
            </Tabs>
        );
    }
}


export default Leaderboard;
