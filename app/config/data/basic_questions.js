module.exports = {
    data: [
        {
            ph: "Ano?",
            type: "interjection",
            en: "What?",
            ph_steps: "Ang hintuturong daliri ay nasa palad, igalaw pailalim.",
            en_steps: "Index finger on open palm, moving downward.",
            image_symbol: require('../../assets/animated-sign-language/basic_questions/ano.gif'),
            audio: require('../../assets/sign-language-audio/basic_questions/ano.wav'),
        },
        {
            ph: "Bakit?",
            type: "interjection",
            en: "Why?",
            ph_steps: "Sa dulo ng baluktot na palad na nakadampi sa noo, igalaw pailalim papunta sa letrang-y na senya.",
            en_steps: "Tips of bent palm touching forehead moving down to y-hand.",
            image_symbol: require('../../assets/animated-sign-language/basic_questions/bakit.gif'),
            audio: require('../../assets/sign-language-audio/basic_questions/bakit.wav'),
        },
        // {
        //     ph: "kailan?",
        //     type: "interjection",
        //     en: "when? ",
        //     ph_steps: "ang hintuturong daliri igalaw sa ibabaw ng isa mo pang daliri.",
        //     en_steps: "index finger moves over resting on tip of other finger.",
        //     image_symbol: require('../../assets/sign-language-symbols/basic_questions/kailan_icon.png'),
        //     audio: require('../../assets/sign-language-audio/basic_questions/kailan.wav'),
        // },
        // {
        //     ph: "kanino?",
        //     type: "interjection",
        //     en: "whose?",
        //     ph_steps: "ang hintuturo ay nasa paikot na galaw sa harap ng iyong bibig palabas na galaw papunta sa letrang-s na senyas.",
        //     en_steps: "index finger in circular motion in front of mouth moving outward to s-hand.",
        //     image_symbol: require('../../assets/sign-language-symbols/basic_questions/kanino_icon.png'),
        //     audio: require('../../assets/sign-language-audio/basic_questions/kanino.wav'),
        // },
        // {
        //     ph: "paano?",
        //     type: "interjection",
        //     en: "how?",
        //     ph_steps: "ibaluktot ang palad, igalaw mula sa loob at saka pataas.",
        //     en_steps: "tip of bent palms turn in and up.",
        //     image_symbol: require('../../assets/sign-language-symbols/basic_questions/paano_icon.png'),
        //     audio: require('../../assets/sign-language-audio/basic_questions/paano.wav'),
        // },
        {
            ph: "Saan?",
            type: "adverb",
            en: "Where? | In what place?",
            ph_steps: "Ang iyong hintuturo ay igalaw ng pabalik-balik.",
            en_steps: "Your index finger back and forth.",
            image_symbol: require('../../assets/animated-sign-language/basic_questions/saan.gif'),
            audio: require('../../assets/sign-language-audio/basic_questions/saan.wav'),
        },
        // {
        //     ph: "sino?",
        //     type: "interjection",
        //     en: "who?",
        //     ph_steps: "ang dulo ng hintuturong daliri igalaw na paikot sa ibabaw ng iyong labi.",
        //     en_steps: "tip of index finger in circular motion over lips.",
        //     image_symbol: require('../../assets/sign-language-symbols/basic_questions/sino_icon.png'),
        //     audio: require('../../assets/sign-language-audio/basic_questions/sino.wav'),
        // }
    ]
}

