import * as JsSearch from 'js-search';
import _ from 'lodash';
import React, { Component } from 'react';
import { Button, Dimensions, Image, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import TimerEnhance from 'react-native-smart-timer-enhance';
import Icon from 'react-native-vector-icons/FontAwesome';
import Voice from 'react-native-voice';

const width = Dimensions.get('window').width; //full width
const uuidv1 = require('uuid/v1');
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const dictionary = require('../../config/data/dictionary');
const ITEMS_PER_PAGE = 3;
const sentenceCase = require('sentence-case');
const search = new JsSearch.Search('ph');
const animal_signs = require('../../config/data/animal_signs');
const basic_questions = require('../../config/data/basic_questions');
const beginning_signs = require('../../config/data/beginning_signs');
const body_parts = require('../../config/data/body_parts');
const behavior_and_safety = require('../../config/data/behavior_and_safety');
const categories = require('../../config/data/categories');
const clothing_signs = require('../../config/data/clothing_signs');
const family_members = require('../../config/data/family_members');
const feelings_and_emotions = require('../../config/data/feelings_and_emotions');
const games_and_activities = require('../../config/data/games_and_activities');
const good_manners = require('../../config/data/good_manners');
const good_grooming = require('../../config/data/good_grooming');
const outdoor_signs = require('../../config/data/outdoor_signs');
const months = require('../../config/data/months');
const days = require('../../config/data/days');
const snack_signs = require('../../config/data/snack_signs');

search.addIndex('en');

const conjuctions = [
    'and',
    'or',
    'but',
    'nor',
    'so',
    'for',
    'yet',
    'after',
    'although',
    'as',
    'as if',
    'as long',
    'as',
    'because',
    'before',
    'even if',
    'even though',
    'once',
    'since',
    'so that',
    'though',
    'till',
    'unless',
    'until',
    'what',
    'when',
    'whenever',
    'wherever',
    'whether',
    'while',
]

const ResultChip = props => (
    <TouchableOpacity onPress={props.onPress} style={{ alignSelf: 'flex-start', margin: 5, backgroundColor: 'rgba(0,0,0,0.1)', padding: 10, borderRadius: 10 }}>
        <Text>{props.data.ph}</Text>
    </TouchableOpacity>
)

const ConjunctionChip = props => (
    <View style={{ alignSelf: 'flex-start', margin: 5, backgroundColor: 'rgba(0,0,0,0.1)', padding: 10, borderRadius: 10, opacity: 0.3 }}>
        <Text>{props.text}</Text>
    </View>
)

const defaultImage = require('../../assets/main.jpg')
const activeTimer = null
class Translator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
            // TODO: Transfer to separate file
            resultsVisible: true,
            recording: false,
            recordingState: 'Recording your voice...',
            imageRef: defaultImage,
            searchResults: [],
            timeframeData: {},
            paused: false,
            playing: false,
            key: uuidv1(),
            reset: false,
            currentImageIndex: 0,
            timer: null
        }
        Voice.onSpeechStart = this.onSpeechStartHandler.bind(this);
        Voice.onSpeechEnd = this.onSpeechEndHandler.bind(this);
        Voice.onSpeechResults = this.onSpeechResultsHandler.bind(this);

    }
    onSpeechStartHandler(e) {
        this.setState({ recording: true })
    }
    onSpeechEndHandler(e) {
        this.setState({ recordingState: 'Processing request...' })
    }
    onSpeechResultsHandler(e) { 
        let result = e.value[0];
        this.setState({ recording: false, recordingState: 'Detected ' + result })
        this.setState({ searchTerm: result })
        console.log("Detected: " + result)
        this.searchUpdated("play")
    }

    onStartVoiceButtonPress = (e) => {
        Voice.start('en-US');
    }

    togglePause() {
        const { timer, paused, playing } = this.state;
        this.setState({paused:!paused, playing: !playing})
    }

    searchUpdated(mode) {
        if (mode === "play") {
            const { searchTerm } = this.state
            if (searchTerm.length < 1) {
                alert("Search box empty!")
            } else {
                const textArray = searchTerm.split(" ")
                const chips = []
                timeframeData = []

                textArray.map(i => {
                    // conjuction detector
                    const conjuction = !(_.findIndex(conjuctions, o => o == _.toLower(i)) == -1)

                    if (!conjuction) {
                        const res = search.search(i)
                        if (!res.length == 0) {
                            // send clickable or non-clickable props
                            //res[0]
                            chips.push(<ResultChip onPress={this.displayChipData.bind(this, res[0])} key={uuidv1()} data={res[0]} />)
                            timeframeData.push(
                                res[0].image_symbol
                            )
                        } else {
                            chips.push(<ConjunctionChip key={uuidv1()} text={i} />)
                            timeframeData.push(
                                defaultImage
                            )
                        }
                    } else {
                        chips.push(<ConjunctionChip key={uuidv1()} text={i} />)
                    }

                })

                this.setState({ searchResults: chips, playing: true })                
                this.updateTimeframe(timeframeData)
            }

            
        }
    }

    updateTimeframe(data) {
        let index = 0
        const duration = data.length - 1
        const timer = this.setInterval(() => {
            const { paused } = this.state
            if (index < duration) {
                index += 1
            } else {
                index = 0
            }
            
            this.setState({ imageRef: data[index] })
            console.log(paused)
            if (paused) {
                clearInterval(timer)
            }
            // if (reset) {
            //     clearInterval(timer)
            //     index = 0
            //     this.setState({ imageRef: defaultImage, searchTerm: '', searchResults: [], playing: false })
            // }
            // console.log(reset)
        }, 1500)

        this.setState({timer})
    }



    displayChipData(data) {
        this.setState({ imageRef: data.image_symbol })
    }

    reset() {
        this.setState({ searchResults: [], searchTerm: ''  })
    }

    _keyExtractor = () => uuidv1();


    componentDidMount() {
        const data = []
        animal_signs.data.map(i => {
            data.push(i)
        })
        basic_questions.data.map(i => {
            data.push(i)
        })
        beginning_signs.data.map(i => {
            data.push(i)
        })
        behavior_and_safety.data.map(i => {
            data.push(i)
        })
        body_parts.data.map(i => {
            data.push(i)
        })
        clothing_signs.data.map(i => {
            data.push(i)
        })
        family_members.data.map(i => {
            data.push(i)
        })
        feelings_and_emotions.data.map(i => {
            data.push(i)
        })
        games_and_activities.data.map(i => {
            data.push(i)
        })
        good_manners.data.map(i => {
            data.push(i)
        })
        good_grooming.data.map(i => {
            data.push(i)
        })
        outdoor_signs.data.map(i => {
            data.push(i)
        })
        snack_signs.data.map(i => {
            data.push(i)
        })
        months.data.map(i => {
            data.push(i)
        })
        days.data.map(i => {
            data.push(i)
        })

        data.forEach(element => {
            search.addDocument(element)
        });
    }
    render() {
        const { key, searchTerm, imageRef, playing, searchResults, recording, recordingState, menuItems, menuLogoVisible, resultsVisible, mode, paused } = this.state;
        return (
            <KeyboardAwareScrollView>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        {
                            recording ?
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: '20%' }}>
                                    <Animatable.Text animation="pulse" iterationCount="infinite">
                                        <Icon name="microphone" style={{ fontSize: 100 }} />
                                    </Animatable.Text>
                                    <Text style={{ margin: 20 }}>{recordingState}</Text>
                                    <Button color="#4CAF50" title="Cancel" onPress={() => {
                                        this.setState({ recording: false })
                                    }} />
                                </View> :
                                <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                                    <View style={{ padding: 0, flex: 1 }}>
                                        <StatusBar backgroundColor={colors.primaryDarker} />
                                        <ScrollView horizontal={true} style={{ padding: 10, flexDirection: 'row' }}>
                                            {
                                                searchResults.length === 0 ?
                                                    <TouchableOpacity style={{ alignSelf: 'flex-start', margin: 5, backgroundColor: 'rgba(0,0,0,0.1)', padding: 10, borderRadius: 10 }}>
                                                        <Text>No match found.</Text>
                                                    </TouchableOpacity>
                                                    :
                                                    searchResults
                                            }
                                        </ScrollView>
                                        <View style={{ alignContent: 'center' }}>
                                            <Image style={{ height: width, width: width }} source={imageRef} resizeMode="contain" />
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10 }}>
                                            <TextInput value={searchTerm} onChangeText={(text) => this.setState({ searchTerm: text })} style={{ flex: 1 }} underlineColorAndroid="transparent" placeholder="Please type a/n english word/s or phrase/s" />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', backgroundColor: '#4CAF50', justifyContent: 'center', alignItems: 'center', padding: 20, margin: 0 }}>

                                        <TouchableOpacity style={[{ backgroundColor: '#FFF', justifyContent: 'center', width: 40, height: 40, borderRadius: 20, alignContent: 'center', margin: 5 }]} onPress={this.onStartVoiceButtonPress}>
                                            <Icon name="microphone" style={[styles.navIconDefault, { color: '#4CAF50', textAlign: 'center', fontSize: 20 }]} />
                                        </TouchableOpacity>
                                     
                                        {
                                            playing? <TouchableOpacity style={[{ backgroundColor: '#FFF', justifyContent: 'center', width: 60, height: 60, borderRadius: 30, alignContent: 'center', margin: 5 }]} onPress={()=>this.togglePause()}>
                                                <Icon name="pause" style={[styles.navIconDefault, { color: '#4CAF50', textAlign: 'center', fontSize: 30 }]} />
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={()=>this.searchUpdated("play")} style={[{ backgroundColor: '#FFF', justifyContent: 'center', width: 60, height: 60, borderRadius: 30, alignContent: 'center', margin: 5 }]}>
                                            <Icon name="play" style={[styles.navIconDefault, { color: '#4CAF50', textAlign: 'center', fontSize: 30 }]} />
                                        </TouchableOpacity>
                                        }
                                        <TouchableOpacity onPress={this.searchUpdated.bind(this)} style={[{ backgroundColor: '#FFF', justifyContent: 'center', width: 40, height: 40, borderRadius: 20, alignContent: 'center', margin: 5 }]} onPress={()=>this.reset()}>
                                            <Icon name="refresh" style={[styles.navIconDefault, { color: '#4CAF50', textAlign: 'center', fontSize: 20 }]} />
                                        </TouchableOpacity>

                                    </View>
                                </View>
                        }
                    </View>
                </ScrollView>
            </KeyboardAwareScrollView>
        )
    }
}

export default TimerEnhance(Translator)