import { StyleSheet, Dimensions } from 'react-native';
import { Platform } from 'react-native';

const colors = require('./colors.js');
const width = Dimensions.get("screen").width;
const height = Dimensions.get("screen").height;
module.exports = StyleSheet.create({
    welcomeContainer: {
        backgroundColor: colors.primary,
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    },
    welcomeLogo: {
        height: 150,
        width: 150
    },
    welcomeTitle: {
        color: 'white',
        margin: 20,
        fontSize: 30,
        marginTop: 50
    },
    welcomeAppTitle: {
        color: 'white',
        fontSize: 60
    },
    textWhite: {
        color: 'white'
    },
    welcomeStartButton: {
        marginTop: 50,
        padding: 15,
        backgroundColor: 'white',
        paddingLeft: 25,
        paddingRight: 25,
        borderRadius: 5
    },
    navBar: {
        backgroundColor: colors.primary
    },
    navTitle: {
        color: 'white',
        fontWeight: '200',
        fontSize: 20
    },
    navIconDefault: {
        color: 'white',
        fontSize: 25
    },
    soundButtonIcon: {
        color: colors.primary,
        fontSize: 50
    },
    homeHeaderImage: {
        height: 150,
        justifyContent: 'center'
    },
    drawerHeaderImage: {
        height: 160,
        justifyContent: 'center',
        padding: 20
    },
    drawerHeader: {
        height: 160,
        justifyContent: 'center',
        padding: 20,
        backgroundColor: colors.primary
    },
    searchInput: {
        paddingLeft: 20,
        paddingRight: 20,
        fontSize: 18
    },
    menuItem: {
        backgroundColor: 'white',
        marginBottom: 0,
        padding: 10
    },
    splashView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    },
    loadingActivity: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    drawerContainer: {
        flex: 1,
        backgroundColor: '#FFF',
        paddingBottom: 10
    },
    photoURL: {
        borderRadius: 30,
        height: 60,
        width: 60
    },
    drawerItem: {
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    drawerItemText: {
        fontSize: 15,
        marginRight: 20
    },
    drawerIcon: {
        fontSize: 20,
        marginRight: 20,
        color: colors.primary
    },
    card: {
        backgroundColor: colors.white,
        borderRadius: 2,
        shadowColor: "#000000",
        shadowOpacity: 0.3,
        shadowRadius: 1,
        shadowOffset: {
            height: 1,
            width: 0.3,
        },
        marginBottom: 5,
        marginTop: 5
    },
    cardImage: {
        flex: 1
    },
    cardTitle: {
        flex: 1,
        flexDirection: 'row',
        padding: 16,
        color: colors.white,
        fontSize: 25,
        marginTop: 80
    },
    cardContent: {
        paddingRight: 16,
        paddingLeft: 16,
        paddingTop: 16,
        paddingBottom: 16,
    },
    cardAction: {
        margin: 8,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5

    },
    separator: {
        flex: 1,
        height: 1,
        backgroundColor: '#E9E9E9'
    },
    fullScreen: {
        width: width,
        height: height,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    wrapper: {
        alignItems: 'center',
        flex: 1
    },
    swiperWrapper: {
        flex: 1,
        height: '100%',
        width: '100%',
        alignSelf: 'stretch'
    },
    slideContainer: {
        flex: 1,
        alignSelf: 'stretch',
        margin: 10,
        marginBottom: 60,
        borderRadius: 10,
        padding: 20,
        alignItems: 'center'
      },
      background: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        borderWidth: 0
      },
})
