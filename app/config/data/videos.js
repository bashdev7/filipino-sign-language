import React, { Component } from 'react';

const animal_signs = require('./animal_signs');
const basic_questions = require('./basic_questions');
const beginning_signs = require('./beginning_signs');
const behavior_and_safety = require('./behavior_and_safety');
const categories = require('./categories');
const clothing_signs = require('./clothing_signs');
const family_members = require('./family_members');
const feelings_and_emotions = require('./feelings_and_emotions');
const games_and_activities = require('./games_and_activities');
const good_manners = require('./good_manners');
const good_grooming = require('./good_grooming');
const outdoor_signs = require('./outdoor_signs');
const snack_signs = require('./snack_signs');

const uuidv1 = require('uuid/v1');
const colors = require('../../config/colors.js');
const sentenceCase = require('sentence-case');

module.exports = {
    data: [
        {
            id: uuidv1(),
            title: 'Basic',
            src: require('../../assets/sign-language-videos/basic.mp4')
        },
        {
            id: uuidv1(),
            title: 'Verbs',
            src: require('../../assets/sign-language-videos/verbs.mp4')
        },
        {
            id: uuidv1(),
            title: 'Adjectives',
            src: require('../../assets/sign-language-videos/adjectives.mp4')
        },
        {
            id: uuidv1(),
            title: 'Fruits',
            src: require('../../assets/sign-language-videos/fruits.mp4')
        },
        {
            id: uuidv1(),
            title: 'Greetings',
            src: require('../../assets/sign-language-videos/greetings.mp4')
        },
        {
            id: uuidv1(),
            title: 'Persons (Part 1)',
            src: require('../../assets/sign-language-videos/persons1.mp4')
        },
        {
            id: uuidv1(),
            title: 'Persons (Part 2)',
            src: require('../../assets/sign-language-videos/persons2.mp4')
        },
        {
            id: uuidv1(),
            title: 'Places',
            src: require('../../assets/sign-language-videos/places.mp4')
        },
        {
            id: uuidv1(),
            title: 'Questions',
            src: require('../../assets/sign-language-videos/questions.mp4')
        },
        {
            id: uuidv1(),
            title: 'Survival',
            src: require('../../assets/sign-language-videos/survival.mp4')
        },
        {
            id: uuidv1(),
            title: 'Vegetables',
            src: require('../../assets/sign-language-videos/vegetables.mp4')
        }
    ]
}