import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../config/styles');

export default class MenuButton extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={[{ marginLeft: 15 }, this.props.style]} onPress={() => Actions.drawerOpen()}>
                <Icon name="menu" style={styles.navIconDefault} />
            </TouchableOpacity>
        )
    }
}