import React, { Component } from 'react';
import { ActivityIndicator, Image, ImageBackground, ScrollView, StatusBar, Text, View } from 'react-native';
import RNFirebase from 'react-native-firebase';
import { ActionConst, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const FBSDK = require('react-native-fbsdk');
const { LoginButton, ShareDialog, LoginManager, AccessToken } = FBSDK;
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
// const firebase = RNFirebase.app();
const settings = require('../../config/settings.js');
const firebase = RNFirebase.app();

export default class Login extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading:false
    }
  }

  loginWithFacebook() {
    const app = this;
    LoginManager
      .logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
          return Promise.reject(new Error('The user cancelled the request'));
        }

        console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
        // get the access token
        return AccessToken.getCurrentAccessToken();
      })
      .then(data => {
        // create a new firebase credential with the token
        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

        // login with credential
        app.setState({ loading: true })
        return firebase.auth().signInWithCredential(credential);
      })
      .then((currentUser) => {
        console.log(currentUser);
        Actions.onboarding({ type: ActionConst.REPLACE })
      })
      .catch((error) => {
        console.log(`Login fail with error: ${error}`);
      });
  }

  loginAnonymously() {
    const app = this;
    app.setState({ loading: true });
    firebase.auth().signInAnonymously().catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage);
      // ...
    });
  }

  loginAnonymouslyOffline() {
    Actions.onboarding({type: ActionConst.REPLACE})
  }

  render() {
    const {loading} = this.state;
    return (
      <ScrollView contentContainerStyle={styles.welcomeContainer}>
      <StatusBar backgroundColor={colors.primaryDarker} />
        <ImageBackground style={{ flex: 1, alignItems: 'center', alignContent: 'center', flexDirection: 'row' }} source={require('../../assets/welcome-bg.png')}>
          <View style={{flexDirection: 'column', alignItems: 'center', flex:1}}>
          <Image style={styles.welcomeLogo} source={require('../../assets/app-logo.png')} resizeMode="contain" />
          <Text style={styles.welcomeAppTitle}>FSL</Text>
          {/* <Text style={[{ fontSize: 20 }, styles.textWhite]}>Filipino Sign Language</Text> */}
          <Text style={[styles.textWhite]}>An Offline Learners' Aid </Text>
          <Text style={[styles.textWhite]}> for Filipino Sign Language</Text>         
          <View style={{margin:20}}>
          <Icon.Button style={{ padding: 10}} name="facebook" backgroundColor="#3b5998" onPress={() => this.loginWithFacebook()}>
            Login with Facebook
          </Icon.Button>
            </View>
          <Icon.Button style={{ padding: 10}} name="user" color="#333" backgroundColor="#fff" onPress={() => this.loginAnonymouslyOffline()}>
            Offline Login
          </Icon.Button>  
          </View>          
        </ImageBackground>
        {loading && 
        <View style={styles.loadingActivity}>
          <ActivityIndicator size='large' />
        </View>
      }
      </ScrollView>
    )
  }
}
