import PropTypes from 'prop-types';
import React from 'react';
import { Alert, Image, ScrollView, Text, TouchableHighlight, View, ViewPropTypes } from 'react-native';
import RNFirebase from 'react-native-firebase';
import { ActionConst, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../config/styles');
const colors = require('../config/colors');
const settings = require('../config/settings.js');
const firebase = RNFirebase.initializeApp(settings.firebase);

export default class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string
  }

  static contextTypes = {
    drawer: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      displayName: "Anonymous User",
      photoURL: require('../assets/empty-profile.png')
    }
  }

  signOut = () => {
    Alert.alert('Filipino Sign Language', "Sign out of FSL?", [
      {
        text: 'Cancel',
        onPress: () => { }
      }, {
        text: 'Sign Out',
        onPress: () => {
          /** FIXME: Bug here. Maybe causing the random crash.
              */
          firebase.auth().signOut().then(() => {
            console.log('You have been signed out');
          }).catch(err => console.error('Uh oh... something bad happened'));

          //TODO: Handle anonymous properly
          Actions.login({type: ActionConst.REPLACE})
        }
      }
    ]);
  }

  componentDidMount() {
    const app = this;
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        if (!user.isAnonymous) {
          app.setState({
            displayName: user.displayName,
            photoURL: { uri: user.photoURL }
          })
        }
      }
    });


  }

  render() {
    const { displayName, photoURL } = this.state;
    return (
      <ScrollView>
        <View style={styles.drawerContainer}>
          <View style={styles.drawerHeader}>
            <View>
              <Image source={require('../assets/app-logo.png')} style={{ width: 70, height: 70, marginBottom: 15}} />
              <Text style={{ fontWeight: 'bold', color: "#FFF" }}>Filipino Sign Language</Text>
              {/* <Image style={[styles.photoURL, { marginBottom: 15 }]} source={photoURL} />
              <Text style={{ fontWeight: 'bold', color: "#FFF" }}>{displayName}</Text> */}
            </View>
          </View>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.home({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="home" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Home</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.favorites({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="heart" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Favorites</Text>
            </View>
          </TouchableHighlight>
          <View style={{ height: 1, borderBottomColor: 'rgba(0,0,0,0.2)', borderBottomWidth: 0.5 }}></View>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.dictionary()}>
            <View style={styles.drawerItem}>
              <Icon name="book" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Dictionary</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.categories({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="list" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Categories</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.extras({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="star" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Extras</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.feedback({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="message-circle" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Feedback/Comments</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.about({ type: ActionConst.REPLACE })}>
            <View style={styles.drawerItem}>
              <Icon name="help-circle" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >About</Text>
            </View>
          </TouchableHighlight>
          {/* <TouchableHighlight underlayColor="rgba(0,0,0,0.1)" onPress={() => this.signOut()}>
            <View style={styles.drawerItem}>
              <Icon name="log-out" style={styles.drawerIcon} />
              <Text style={styles.drawerItemText} >Logout</Text>
            </View>
          </TouchableHighlight> */}
        </View >
      </ScrollView>
    );
  }
}
