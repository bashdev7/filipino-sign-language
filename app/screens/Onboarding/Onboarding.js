import React, { Component } from 'react';
import { AsyncStorage, Image, ImageBackground, StatusBar, Text, View } from 'react-native';
import { ActionConst, Actions } from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = require('../../config/styles')
const logo = require('../../assets/app-logo.png')
const fslToday = require('../../assets/fsl-today.png')
const fslDefinition = require('../../assets/fsl-definition.png')
const fslAudio = require('../../assets/fsl-audio.png')
const fslVideo = require('../../assets/fsl-video.png')
const fslGames = require('../../assets/fsl-games.png')
const fslTranslator = require('../../assets/fsl-translator.png')


const dot = (<View style={{
    backgroundColor: 'rgba(255,255,255,0.5)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
}} />)

const activeDot = (<View style={{
    backgroundColor: 'rgba(255,255,255,1)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
}} />)

const nextButton = (
    <Icon name="caret-right" color="#FFF" size={30} />
)

const prevButton = (
    <Icon name="caret-left" color="#FFF" size={30} />
)

export default class Onboarding extends Component {
    finish() {
        try {
            AsyncStorage.setItem('onboardingDone', 'true').then(() => Actions.main({ type: ActionConst.RESET }));
        } catch (error) {
            console.log("Error saving data");
        }
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <StatusBar backgroundColor="#333" />
                <ImageBackground style={styles.background} source={require('../../assets/welcome-bg-dark.png')}>
                    <Swiper style={styles.wrapper} showsButtons loop={false} dot={dot} activeDot={activeDot} nextButton={nextButton} prevButton={prevButton} containerStyle={styles.swiperWrapper}>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={logo} style={{ width: 150, height: 150, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Welcome to FSL</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Learn the Filipino Sign Language without going to PWD schools.</Text>
                                <View style={{ margin: 20, alignItems: 'flex-end', alignContent: 'flex-end' }}>
                                <Icon.Button style={{ paddingLeft: 20, paddingRight: 20 }} name="step-forward" onPress={() => Actions.home({ type: ActionConst.REPLACE })}>
                                    Skip
                        </Icon.Button>
                            </View>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={fslToday} style={{ width: 300, height: 150, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>FSL of the Day</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Learn a new sign language every day.</Text>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={fslDefinition} style={{ width: 150, height: 150, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Quick Definitions</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Quickly browse the FSL you're looking for - perfect for on-the-go lookups.</Text>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={fslTranslator} style={{ width: 130, height: 130, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Translator</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Lookup terms and know their FSL translations.</Text>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={fslAudio} style={{ width: 130, height: 130, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Real Filipino Speakers</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Listen to recorded audio pronunciations from real filipino speakers - not robots.</Text>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={fslVideo} style={{ width: 130, height: 130, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Offline Video Tutorials</Text>
                                <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 15 }}>Watch how the professionals perform the Filipino Sign Language.</Text>
                            </View>
                        </View>
                        <View style={[styles.slideContainer, { alignContent: 'center', alignItems: 'center', flexDirection: 'row' }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center', padding: 20 }}>
                                <Image source={fslGames} style={{ width: 200, height: 100, marginBottom: 30 }} resizeMode="contain" />
                                <Text style={{ color: "#FFF", fontSize: 25, fontWeight: 'bold' }}>Mini Games</Text>
                                <Text style={{ textAlign: 'center', marginBottom: 50, color: '#FFF', fontSize: 15 }}>Learn FSL by playing our mini games.</Text>
                                <Icon.Button style={{ padding: 10 }} name="play" color="green" backgroundColor="#fff" onPress={() => Actions.home({ type: ActionConst.REPLACE })}>
                                    Get Started
                                </Icon.Button>
                            </View>
                        </View>
                    </Swiper>
                </ImageBackground>
            </View>
        )
    }
}