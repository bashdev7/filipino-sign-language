import React, { Component } from 'react';
import { Button, Dimensions, Image, ScrollView, Text, View } from 'react-native';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const width = Dimensions.get('window').width; //full width

export default class AnimatedSignLanguage extends Component {
    render(){
        return (
            <ScrollView contentContainerStyle={{backgroundColor: 'white'}}>
            <View style={{flex:1, padding: 0}}>
                <Image style={{width: width, height: width*1}} source={require('../../assets/animated-sign-language/1.gif')} resizeMode="contain" />
                <View style={{flex:1, alignItems: 'flex-start', padding: 20}}>
                <Text style={{fontSize: 20}}>How are you? (Kamusta)</Text>
                <Text>Say Kumustá to greet somebody your age or younger. Say Kumustá  any time of day. Say it to people you just met. Say it to people you already know. Kumustá means both “Hello” and “How are you?” </Text>
                </View>
                <View style={{padding: 20, paddingTop: 0}}>
                <Button onPress={()=>alert("Not yet available.")}title="WATCH VIDEO" color={colors.primary}/>
                </View>
            </View>
            
            </ScrollView>
        )
    }
}