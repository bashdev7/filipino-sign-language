import React, { Component } from 'react';
import { Actions, Drawer, Reducer, Router, Scene, Stack } from 'react-native-router-flux';

import BackButton from '../components/BackButton';
import CloseButton from '../components/CloseButton';
import DrawerContent from '../components/DrawerContent';
import MenuButton from '../components/MenuButton';
import About from './About/About';
import Login from './Auth/Login';
import Categories from './Categories/Categories';
import CategoryContent from './Categories/CategoryContent';
import Dictionary from './Dictionary/Dictionary';
import Meaning from './Dictionary/Meaning';
import Extras from './Extras/Extras';
import Favorites from './Favorites/Favorites';
import Feedback from './Feedback/Feedback';
import Home from './Home/Home';
import MiniGameCategories from './MiniGame/Categories';
import Easy from './MiniGame/Easy';
import Hard from './MiniGame/Hard';
import Leaderboard from './MiniGame/Leaderboard';
import MiniGame from './MiniGame/MiniGame';
import Panic from './MiniGame/Panic';
import Start from './MiniGame/Start';
import Onboarding from './Onboarding/Onboarding';
import Search from './Search/Search';
import Splash from './Splash/Splash';
import Translator from './Translator/Translator';
import Player from './Videos/Player';
import Videos from './Videos/Videos';

// Components
// Config
const styles = require('../config/styles');
const colors = require('../config/colors');

// Screens
const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        console.log('ACTION:', action);
        return defaultReducer(state, action);
    };
};

export default class Screens extends Component {
    onBackPress = () => {
        if (Actions.state.index === 0) {
            return false
        }
        Actions.pop()
        return true
    }

    render() {
        return (
            <Router
                createReducer={reducerCreate}
            >
                <Stack
                    hideNavBar
                    key="root"
                >

                    <Drawer
                        hideNavBar
                        key="drawer"
                        contentComponent={() => <DrawerContent />}
                        drawerWidth={270}
                        backAndroidHandler={this.onBackPress}
                    >
                        <Stack key="root" hideNavBar>
                            <Scene
                                key="splash"
                                component={Splash}
                                hideNavBar={true}
                            />
                            <Scene
                                key="onboarding"
                                component={Onboarding}
                                hideNavBar={true}
                            />
                            <Scene
                                key="home"
                                component={Home}
                                title="Home"
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                hideNavBar={true}
                            />
                            <Scene
                                key="login"
                                component={Login}
                                hideNavBar={true}
                            />
                            <Scene
                                key="search"
                                component={Search}
                                hideNavBar={true}
                            />
                            <Scene
                                key="dictionary"
                                component={Dictionary}
                                hideNavBar={true}
                            />
                            <Scene
                                key="meaning"
                                component={Meaning}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><CloseButton />}
                                title="Meaning"                                
                            />
                            <Scene
                                key="category_content"
                                component={CategoryContent}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><BackButton />}
                                title="Content"                                
                            />
                            <Scene
                                key="videos"
                                component={Videos}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><BackButton />}
                                title="Video Tutorials"                                
                            />
                            <Scene
                                key="player"
                                component={Player}
                                hideNavBar={true}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><BackButton />}
                                title="Player"                                
                            />
                            <Scene
                                key="favorites"
                                component={Favorites}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Favorites"                                
                            />
                            <Scene
                                key="minigame"
                                component={MiniGame}
                                hideNavBar={true}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="FSL MiniGame"                                
                            />
                            <Scene
                                key="startgame"
                                component={Start}
                                hideNavBar={true}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="FSL MiniGame"                                
                            />
                            <Scene
                                key="startgamecategories"
                                component={MiniGameCategories}
                                hideNavBar={true}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="FSL MiniGame"                                
                            />
                            <Scene
                                key="easy"
                                component={Easy}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Mini Game (Easy)"                                
                            />
                            <Scene
                                key="hard"
                                component={Hard}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Mini Game (Hard)"                                
                            />
                            <Scene
                                key="panic"
                                component={Panic}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Mini Game (Panic)"                                
                            />
                            <Scene
                                key="leaderboard"
                                component={Leaderboard}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Leaderboard"                                
                            />
                            <Scene
                                key="categories"
                                component={Categories}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Categories"                                
                            />
                            <Scene
                                key="extras"
                                component={Extras}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Extras"                                
                            />
                            <Scene
                                key="about"
                                component={About}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="About"                                
                            />
                            <Scene
                                key="feedback"
                                component={Feedback}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Feedback"                                
                            />
                            <Scene
                                key="translator"
                                component={Translator}
                                hideNavBar={false}
                                navigationBarStyle={styles.navBar}
                                titleStyle={styles.navTitle}
                                renderLeftButton={()=><MenuButton />}
                                title="Translator"                                
                            />


                        </Stack>
                    </Drawer>

                </Stack>
            </Router>
        )
    }
}
