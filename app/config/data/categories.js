import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import Alphabets from '../../components/ScreenCategories/Alphabets';
import GeneralCategory from '../../components/ScreenCategories/GeneralCategory';
import Numbers from '../../components/ScreenCategories/Numbers';

const animal_signs = require('./animal_signs')
const basic_questions = require('./basic_questions')
const beginning_signs = require('./beginning_signs')
const behavior_and_safety = require('./behavior_and_safety')
const categories = require('./categories')
const clothing_signs = require('./clothing_signs')
const family_members = require('./family_members')
const feelings_and_emotions = require('./feelings_and_emotions')
const games_and_activities = require('./games_and_activities')
const good_manners = require('./good_manners')
const months = require('./months')
const days = require('./days')
const good_grooming = require('./good_grooming')
const outdoor_signs = require('./outdoor_signs')
const snack_signs = require('./snack_signs')

const uuidv1 = require('uuid/v1')
const colors = require('../../config/colors.js')
const sentenceCase = require('sentence-case')

module.exports = {
    data: [
        {
            id: uuidv1(),
            title: 'Words/Phrases',
            action: ()=>Actions.dictionary(),
            color: colors.indigo.a700,
            desc: "Filipino phrases like kamusta ka and other most commonly used words."
        }, {
            id: uuidv1(),
            title: 'Alphabets',
            action: ()=>Actions.category_content({content: <Alphabets />, title: 'Alphabets'}),
            color: colors.purple.a700,
            desc: "Learn more about the Filipino Alphabet."
        }, 
        {
            id: uuidv1(),
            title: 'Months',
            action: ()=>Actions.category_content({content: <GeneralCategory category={months.data} />, title: 'Months'}),
            color: colors.green.a700,
            desc: "Learn more about the Filipino Calendar Sign Language"
        },
        {
            id: uuidv1(),
            title: 'Week Days',
            action: ()=>Actions.category_content({content: <GeneralCategory category={days.data} />, title: 'Week Days'}),
            color: colors.green.a700,
            desc: "Learn more about the Filipino Week Days Sign Language"
        },
        {
            id: uuidv1(),
            title: 'Numbers',
            action: ()=>Actions.category_content({content: <Numbers />, title: 'Numbers'}),
            color: colors.orange.a700,
            desc: "Learn more about the Filipino Numbers."
        },  
        {
            id: uuidv1(),
            title: 'Animal Signs',
            action: ()=>Actions.category_content({content: <GeneralCategory category={animal_signs.data} />, title: 'Animal Signs'}),
            color: colors.cyan.a700,
            desc: "Filipino Sign Language actions for animals."
        }, {
            id: uuidv1(),
            title: 'Basic Questions',
            action: ()=>Actions.category_content({content: <GeneralCategory category={basic_questions.data} />, title: 'Basic Questions'}),
            color: colors.blue.a700,
            desc: "Asking basic questions."
        }, 
        {
            id: uuidv1(),
            title: 'Games and Activities',
            action: ()=>Actions.category_content({content: <GeneralCategory category={games_and_activities.data} />, title: 'Games and Activities'}),
            color: colors.green.a200,
            desc: "FSL for sports and other activities."
        }, 
        {
            id: uuidv1(),
            title: 'Beginning Signs',
            action: ()=>Actions.category_content({content: <GeneralCategory category={beginning_signs.data} />, title: 'Beginning Signs'}),
            color: colors.orange.a700,
            desc: "Learn to start a conversation.."
        }, {
            id: uuidv1(),
            title: 'Behavior and Safety',
            action: ()=>Actions.category_content({content: <GeneralCategory category={behavior_and_safety.data} />, title: 'Behavior and Safety'}),
            color: colors.red.a700,
            desc: "Safety first FSL!"
        }, {
            id: uuidv1(),
            title: 'Clothing Signs',
            action: ()=>Actions.category_content({content: <GeneralCategory category={clothing_signs.data} />, title: 'Clothing Signs'}),
            color: colors.purple.a700,
            desc: "Garments..etc."
        }, {
            id: uuidv1(),
            title: 'Family Members',
            action: ()=>Actions.category_content({content: <GeneralCategory category={family_members.data} />, title: 'Family Members'}),
            color: colors.teal.a700,
            desc: "Filipinos are well known for strong family ties."
        }, {
            id: uuidv1(),
            title: 'Feelings and Emotions',
            action: ()=>Actions.category_content({content: <GeneralCategory category={feelings_and_emotions.data} />, title: 'Feelings and Emotions'}),
            color: colors.cyan.a700,
            desc: "Filipino Sign Language for what you feel."
        }, 
        // {
        //     id: uuidv1(),
        //     title: 'Frequently Used Words',
        //     action: ()=>Actions.category_content({content: "x", title: 'Frequently Used Words'}),
        //     color: colors.pink.a700,
        //     desc: "Filipino Sign Language actions for commonly used words."
        // }, 
        // {
        //     id: uuidv1(),
        //     title: 'FSL History',
        //     action: ()=>Actions.category_content({content: <History />, title: 'FSL History'}),
        //     color: colors.brown.a700,
        //     desc: "Learn how FSL started."
        // }, 
        // {
        //     id: uuidv1(),
        //     title: 'Games and Activities',
        //     action: ()=>Actions.category_content({content: <GeneralCategory category={games_and_activities.data} />, title: 'Games and Activities'}),
        //     color: colors.green.a200,
        //     desc: "FSL for sports and other activities."
        // }, 
        {
            id: uuidv1(),
            title: 'Good Grooming',
            action: ()=>Actions.category_content({content: <GeneralCategory category={good_grooming.data} />, title: 'Good Grooming'}),
            color: colors.pink.a200,
            desc: "Filipino Sign Language actions for proper grooming."
        }, {
            id: uuidv1(),
            title: 'Good Manners',
            action: ()=>Actions.category_content({content: <GeneralCategory category={good_manners.data} />, title: 'Good Manners'}),
            color: colors.blue.a200,
            desc: "Filipino Sign Language actions for good conduct and manners."
        }, 
        // {
        //     id: uuidv1(),
        //     title: 'Translator',
        //     action: ()=>Actions.category_content({content: "x", title: 'Translator'}),
        //     color: colors.grey.a700,
        //     desc: "Quickly find the Filipino term meaning and it's quivalent FSL."
        // }, 
        // {
        //     id: uuidv1(),
        //     title: 'Mini Game',
        //     action: ()=>Actions.category_content({content: "x", title: 'Mini Game'}),
        //     color: colors.orange.a400,
        //     desc: "Wan't to test your skills? Play our mini games!"
        // }, 
        // {
        //     id: uuidv1(),
        //     title: 'Numbers',
        //     action: ()=> Alert.alert('Filipino Sign Language', "Sorry, numbers are currently not available."),
        //     color: colors.purple.a400,
        //     desc: "FSL for numbers."
        // }, 
        {
            id: uuidv1(),
            title: 'Outdoor Signs',
            action: ()=>Actions.category_content({content: <GeneralCategory category={outdoor_signs.data} />, title: 'Outdoor Signs'}),
            color: colors.indigo.a200,
            desc: "Learn FSL for outdoor signs."
        }, {
            id: uuidv1(),
            title: 'Snack Signs',
            action: ()=>Actions.category_content({content: <GeneralCategory category={snack_signs.data} />, title: 'Snack Signs'}),
            color: colors.teal.a200,
            desc: "Ready to eat?"
        }, 
        // {
        //     id: uuidv1(),
        //     title: 'Video Tutorials',
        //     action: ()=>Actions.category_content({content: "x", title: 'Video Tutorials'}),
        //     color: colors.red.a400,
        //     desc: "Quick video tutorials to get started."
        // }
    ]
}