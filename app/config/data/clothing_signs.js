module.exports = {
    data: [{
        ph: "Kapote",
        type: "noun",
        en: "Raincoat",
        ph_steps: "Ilagay ang mga kamay paharap na parang nagsusuot ng kapote.",
        en_steps: "Bring both arms forwards as if putting on a coat.",
        image_symbol: require('../../assets/animated-sign-language/clothing_signs/kapote.gif'),
        audio: require('../../assets/sign-language-audio/clothing_signs/kapote.wav'),
    },
    {
        ph: "Medyas",
        type: "noun",
        en: "Socks, Stockings",
        ph_steps: "Ang dalawang hintuturo ay nakaturo pababa ang isa ay pataas at ang isa naman ay pababa, na nagkikiskisan sa isa’t isa.",
        en_steps: "Both index fingers point down. one moves up and the other down, brushing against each other.",
        image_symbol: require('../../assets/animated-sign-language/clothing_signs/mejas.gif'),
        audio: require('../../assets/sign-language-audio/clothing_signs/medyas.wav'),
    },
    {
        ph: "Sapatos",
        type: "noun",
        en: "Shoes",
        ph_steps: "Itapik ang iyong dalawang kamao pababa ng dalawang beses nang magkasabay.",
        en_steps: "Tap your two downward fists together twice.",
        image_symbol: require('../../assets/animated-sign-language/clothing_signs/sapatos.gif'),
        audio: require('../../assets/sign-language-audio/clothing_signs/sapatos.wav'),
    },
    // {
    //     ph: "sombrero",
    //     type: "special noun",
    //     en: "hat",
    //     ph_steps: "ang iyong kamay ay itapik sa ibabaw ng iyong ulo.",
    //     en_steps: "your open hand pats the top of your head.",
    //     image_symbol: require('../../assets/sign-language-symbols/clothing_signs/sombrero_icon.png'),
    //     audio: require('../../assets/sign-language-audio/clothing_signs/sombrero.wav'),
    // }
    ]
}
