module.exports = {
    data: [
        {
            text: 'According to the World Health Organization, as of March 2018 around 466 million people worldwide have disabling hearing loss (1), and 34 million of these are children.'
        },
        {
            text: 'It is estimated that by 2050 over 900 million people will have disabling hearing loss.'
        },
        {
            text: 'Hearing loss may result from genetic causes, complications at birth, certain infectious diseases, chronic ear infections, the use of particular drugs, exposure to excessive noise, and ageing.'
        },
        {
            text: '60% of childhood hearing loss is due to preventable causes.'
        },
        {
            text: '1.1 billion young people (aged between 12–35 years) are at risk of hearing loss due to exposure to noise in recreational setting'
        },
        {
            text: 'Unaddressed hearing loss poses an annual global cost of 750 billion international dollars (2). Interventions to prevent, identify and address hearing loss are cost-effective and can bring great benefit to individuals.'
        },
        {
            text: 'People with hearing loss benefit from early identification; use of hearing aids, cochlear implants and other assistive devices; captioning and sign language; and other forms of educational and social support.'
        },
        {
            text: 'In the Philippines, hearing impairment was reported to be 17% or 97,957 per 577,345 population based on the National Registry of the Department of Health (DOH) in 1997. Moreover, the nationwide survey on hearing disability and ear disorders conducted by Better Hearing Philippines in 2005 estimates the prevalence of hearing disability at 8.8%, while the prevalence of hearing impairment, including mild forms of hearing loss, was at 28%.'
        },
        {
            text: 'According to the Philippine Statistic Authority, about 16 per thousand of the country’s population had disability. Of the 92.1 million household population in the country, 1,443 thousand persons or 1.57 percent had disability, based on the 2010 Census of Population and Housing (2010 CPH). The recorded figure of persons with disability (PWD) in the 2000 CPH was 935,551 persons, which was 1.23 percent of the household population.'
        },
        {
            text: 'Hearing loss is a major public health issue that is the third most common physical condition after arthritis and heart disease'
        },
        {
            text: 'There are degrees of hearing loss: mild, moderate, severe, profound'
        },
        {
            text: 'Different Countries have different Sign Language. '
        },
        {
            text: 'Every sign gesture of the Filipino Sign Language came from the experience and culture of the Filipino'
        },
        {
            text: 'There are at least 4 pending Bill in both Senate and Congress for AN ACT DECLARING THE FILIPINO SIGN LANGUAGE AS THE NATIONAL SIGN LANGUAGE OF THE FILIPINO DEAF AND THE OFFICIAL SIGN LANGUAGE OF GOVERNMENT IN ALL TRANSACTIONS INVOLVING THE DEAF, AND MANDATING ITS USE IN SCHOOLS, BROADCAST MEDIA, AND WORKPLACES.'
        }
    ],
}