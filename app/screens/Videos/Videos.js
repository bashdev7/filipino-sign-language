import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

const videos = require('../../config/data/videos');

const uuidv1 = require('uuid/v1');
const sentenceCase = require('sentence-case');

export default class Videos extends Component {
    _keyExtractor = () => uuidv1();

    render(){
        return (
            <ScrollView style={{backgroundColor:colors.white}}>
                 {videos.data.map(item=>
                        <View key={this._keyExtractor} >                            
                            <TouchableOpacity style={{ paddingLeft: 0, paddingTop: 20, paddingBottom: 20, flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, borderBottomColor:'rgba(0,0,0,0.2)', borderBottomWidth: 0.5 }} onPress={() => Actions.player({src:item.src})}>
                            <Icon name="video" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                            <Text numberOfLines={1} style={{ fontSize: 15, flex:1 }}>{item.title}</Text>
                        </TouchableOpacity>
                        </View>
                        )}
            </ScrollView>
        )
    }
}