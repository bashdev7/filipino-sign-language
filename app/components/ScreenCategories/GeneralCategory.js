import React, { Component } from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const sentenceCase = require('sentence-case');
const styles = require('../../config/styles');
const colors = require('../../config/colors');
const uuidv1 = require('uuid/v1');
export default class GeneralCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.category
        }
    }
    _keyExtractor = () => uuidv1();

    render() {
        const { data } = this.state;
        return (
            <FlatList
                data={data}
                style={{ marginBottom: 5, backgroundColor: 'rgba(255,255,255,0.8)' }}
                renderItem={({ item }) => (
                    <TouchableOpacity style={{ paddingLeft: 0, paddingTop: 15, paddingBottom: 15, flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFF' }} onPress={() => Actions.meaning({ title: sentenceCase(item.ph), data: item })}>
                        <Icon name="book" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                        <Text style={{ fontSize: 15, marginRight: 80 }} numberOfLines={1}>{sentenceCase(item.ph)}</Text>
                    </TouchableOpacity>
                )}
                ItemSeparatorComponent={() =>
                    <View
                        style={{
                            height: 1,
                            width: '100%',
                            backgroundColor: '#F0EEE8',
                        }}
                    />
                }
                keyExtractor={this._keyExtractor}
            />
        )
    }
}