import React, { Component } from 'react';

import Screens from './screens';

export default class App extends Component<{}> {
  render() {
    const { region } = this.props;
    return (
      <Screens />
    );
  }
}

