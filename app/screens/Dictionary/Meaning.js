import React, { Component } from 'react';
import { Dimensions, Image, ScrollView, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import FavButton from '../../components/FavButton';
import SoundButton from '../../components/SoundButton';

const sentenceCase = require('sentence-case');
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const width = Dimensions.get('window').width; //full width

export default class Meaning extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFav:false
        }
    }
    componentDidMount(){
        const {isFav} =  this.state
        Actions.refresh({ extraProp: 'refresh',right:()=><FavButton isFav={isFav} data={this.props.data} /> });
    }
    render() {
        const { data } = this.props;
        return (
            <ScrollView style={{ backgroundColor: '#FFF' }}>
            <View style={{ backgroundColor: '#FFF' }}>
                <View style={{padding: 0}}>
                    <View style={{alignItems:'center', marginBottom: 20}}>
                    <Image style={{height: width, width: width}} source={data.image_symbol} resizeMode="contain"/>
                    </View>
                    <View style={{padding: 20}}>
                    <Text style={{ fontWeight: 'bold', opacity: 0.5 }}>English Translation</Text>
                    <View style={{marginBottom: 0, alignItems:'center', flexDirection:'row'}}>
                    <Text style={{fontSize: 30, color: colors.primary }}>{data.en}</Text>                                       
                    </View>
                    <Text style={{fontSize:15, marginBottom: 20, fontStyle: 'italic'}}>{data.type}</Text> 
                    <Text style={{ fontWeight: 'bold', opacity: 0.5 }}>Audio (Filipino)</Text>
                    <SoundButton style={{marginBottom: 20}} src={data.audio} />
                    <Text style={{ fontWeight: 'bold', opacity: 0.5 }}>How to (English)</Text>
                    <Text style={{ marginBottom: 20, fontSize: 15 }}>{data.en_steps}</Text>
                    <Text style={{ fontWeight: 'bold', opacity: 0.5 }}>How to (Filipino)</Text>
                    <Text style={{fontSize: 15, marginBottom: 20 }}>{data.ph_steps}</Text>
                    </View>
                </View>
            </View>
            </ScrollView>
        )
    }
}