import React, { Component } from 'react';
import { FlatList, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Icon from 'react-native-vector-icons/Feather';

const KEYS_TO_FILTERS = ['title'];
const uuidv1 = require('uuid/v1');
const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');
const categories = require('../../config/data/categories');
export default class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
            // TODO: Transfer to separate file
            menuItems: categories.data
        }
    }

    searchUpdated(term) {
        this.setState({ searchTerm: term })
    }

    _keyExtractor = () => uuidv1();

    render() {
        const { searchTerm, menuItems, menuLogoVisible } = this.state;
        const data = menuItems.filter(createFilter(searchTerm, KEYS_TO_FILTERS))
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderColor: 'rgba(0,0,0,0.2)', paddingTop: 3, paddingBottom: 3 }}>
                    <TouchableOpacity style={[{ marginLeft: 15 }]} onPress={() => Actions.pop()}>
                        <Icon name="arrow-left" style={[styles.navIconDefault, { color: colors.primary }]} />
                    </TouchableOpacity>
                    <View style={{ flex: 1 }}>
                        <SearchInput
                            onChangeText={(term) => { this.searchUpdated(term) }}
                            style={styles.searchInput}
                            placeholder="Search FSL Categories"
                            clearIcon={<Icon name="x" style={{ color: colors.primary, fontSize: 20 }} />}
                            clearIconViewStyles={{ position: 'absolute', top: 15, right: 22 }}
                        />
                    </View>
                </View>
                <StatusBar backgroundColor={colors.primaryDarker} />
                <FlatList
                    data={data}
                    style={{ marginBottom: 5 }}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ paddingLeft: 0, paddingTop: 10, paddingBottom: 10, flexDirection: 'row', alignItems: 'center' }} onPress={() => item.action()}>
                            <Icon name="search" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                            <Text style={{ fontSize: 15, flex:1 }}>{item.title}</Text>
                        </TouchableOpacity>
                    )}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}
