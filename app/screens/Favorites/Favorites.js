import _ from 'lodash';
import React, { Component } from 'react';
import { AsyncStorage, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Feather';

const styles = require('../../config/styles.js');
const colors = require('../../config/colors.js');

// const animal_signs = require('../../config/data/animal_signs');
// const basic_questions = require('../../config/data/basic_questions');
// const beginning_signs = require('../../config/data/beginning_signs');
// const behavior_and_safety = require('../../config/data/behavior_and_safety');
// const categories = require('../../config/data/categories');
// const clothing_signs = require('../../config/data/clothing_signs');
// const family_members = require('../../config/data/family_members');
// const feelings_and_emotions = require('../../config/data/feelings_and_emotions');
// const games_and_activities = require('../../config/data/games_and_activities');
// const good_manners = require('../../config/data/good_manners');
// const good_grooming = require('../../config/data/good_grooming');
// const outdoor_signs = require('../../config/data/outdoor_signs');
// const snack_signs = require('../../config/data/snack_signs');

// const favorites = [].concat(
//     animal_signs.data[2],
//     basic_questions.data[5],
//     clothing_signs.data[0],
//     games_and_activities.data[2],
//     good_manners.data[2],
//     outdoor_signs.data[4]
// );

const uuidv1 = require('uuid/v1');
const sentenceCase = require('sentence-case');

export default class Favorites extends Component {

    constructor(props){
        super(props)
        this.state = {
            favorites: [],
            loading: true
        }
    }

    updateFav(){
        AsyncStorage.getItem('@MySuperStore:favorites').then(value=>{
            if(value!==""){
                this.setState({favorites: JSON.parse(value), loading: false})
            }
        }).catch(err=>{
            console.log(err)
        })
    }

    componentDidMount(){
        setInterval(()=>this.updateFav(), 1500)
    }

    componentWillReceiveProps(){
        this.updateFav()
    }

    removeFavItem(array, key){
        _.remove(array, n => {
            return n.ph == key // use a different key
        })

        AsyncStorage.setItem('@MySuperStore:favorites', JSON.stringify(array)).then(()=>{
            this.updateFav()
            console.log("Updated Fav")
        })
    }
    
    render(){
        const { favorites, loading } = this.state
        return (
            <ScrollView style={{backgroundColor:colors.white}} contentContainerStyle={{flex:1}}>
                 <View style={{flex:1}}>
                     {
                         loading?
                         <View style={{flex:1, alignContent: 'center', alignItems: 'center', justifyContent: 'center', padding: 50}}>
                         <Text style={{textAlign: 'center'}}>Loading favorites...</Text>
                         </View>
                        :
                        JSON.stringify(favorites).length>5?Object.keys(favorites).map(item=>
                            <View key={uuidv1()} >                            
                                <View style={{ paddingLeft: 0, paddingTop: 20, paddingBottom: 20, flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, borderBottomColor:'rgba(0,0,0,0.2)', borderBottomWidth: 0.5}}>
                                <TouchableOpacity style={{flexDirection: 'row', flex: 1}} onPress={() => Actions.meaning({title:sentenceCase(favorites[item].ph), data:favorites[item]})}>
                                <Icon name="bookmark" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                                <Text numberOfLines={1} style={{ fontSize: 15 }}>{favorites[item].ph}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  style={{marginRight: 10}} onPress={()=>this.removeFavItem(favorites,favorites[item].ph)}>
                                <Icon name="x" style={{ fontSize: 20, marginLeft: 20, marginRight: 10 }} />
                                </TouchableOpacity>
                            </View>
                            </View>                            
                        ):
                        <View style={{flex:1, alignContent: 'center', alignItems: 'center', justifyContent: 'center', padding: 50}}>
                            <Text style={{textAlign: 'center'}}>No favorites yet. Open the dictionary to add a favorite FSL.</Text>
                        </View>
                        
                    }
                 </View>
            </ScrollView>
        )
    }
}